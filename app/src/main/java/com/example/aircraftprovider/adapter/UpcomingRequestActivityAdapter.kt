package com.example.aircraftprovider.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.ReviewPayActivities
import com.example.aircraftprovider.adapter.UpcomingRequestActivityAdapter.UpcomingRequestViewHolder
import com.example.aircraftprovider.model.ListModel

class UpcomingRequestActivityAdapter(var context: Context, var listModels: List<ListModel>) :
    RecyclerView.Adapter<UpcomingRequestViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpcomingRequestViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.upcoming_request_adapter_view, parent, false)
        return UpcomingRequestViewHolder(view)
    }

    override fun onBindViewHolder(holder: UpcomingRequestViewHolder, position: Int) {
        holder.date.text = listModels[position].title
        holder.month.text = listModels[position].name
        holder.constraintLayout.setOnClickListener {
            context.startActivity(
                Intent(
                    context,
                    ReviewPayActivities::class.java
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return listModels.size
    }

    inner class UpcomingRequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView
        var month: TextView
        var constraintLayout: ConstraintLayout

        init {
            date = itemView.findViewById(R.id.tv_date_dd)
            month = itemView.findViewById(R.id.tv_date_mm)
            constraintLayout = itemView.findViewById(R.id.constraintlayout)
        }
    }
}