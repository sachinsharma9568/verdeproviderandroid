package com.example.aircraftprovider.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.aircraftprovider.fragments.UpcomingFragment

class PagerAdapter(fm: FragmentManager?, var mNumOfTabs: Int) : FragmentStatePagerAdapter(
    fm!!
) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                UpcomingFragment()
            }
            1 -> {
                UpcomingFragment()
            }
            else -> null
        }
    }

    override fun getCount(): Int {
        return mNumOfTabs
    }
}