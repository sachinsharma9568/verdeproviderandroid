package com.example.aircraftprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.AircraftsDetailAdapter.AircraftsDetailViewHolder

class AircraftsDetailAdapter(var context: Context, var a: Array<Int>) :
    RecyclerView.Adapter<AircraftsDetailViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AircraftsDetailViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.aircraft_detail_adapter_view, parent, false)
        return AircraftsDetailViewHolder(view)
    }

    override fun onBindViewHolder(holder: AircraftsDetailViewHolder, position: Int) {
        holder.imageView.setImageResource(a[position])
    }

    override fun getItemCount(): Int {
        return a.size
    }

    inner class AircraftsDetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView

        init {
            imageView = itemView.findViewById(R.id.image)
        }
    }
}