package com.example.aircraftprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.HomeFragmentUpcomingAdapter.HomeFragmentUpcomingViewHolder
import com.example.aircraftprovider.model.ListModel

class HomeFragmentUpcomingAdapter(var context: Context, var listModels: List<ListModel>) :
    RecyclerView.Adapter<HomeFragmentUpcomingViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HomeFragmentUpcomingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.home_upcoming_adapter_view, parent, false)
        return HomeFragmentUpcomingViewHolder(view)
    }

    override fun onBindViewHolder(holder: HomeFragmentUpcomingViewHolder, position: Int) {
        holder.date.text = listModels[position].title
        holder.month.text = listModels[position].name
        if (position == 1) {
            holder.constraintLayout.setBackgroundResource(R.drawable.lightorange_rectangle_home)
        }
    }

    override fun getItemCount(): Int {
        return listModels.size
    }

    inner class HomeFragmentUpcomingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView
        var month: TextView
        var constraintLayout: ConstraintLayout

        init {
            date = itemView.findViewById(R.id.tv_date_dd)
            month = itemView.findViewById(R.id.tv_date_mm)
            constraintLayout = itemView.findViewById(R.id.newyorkconstraint)
        }
    }
}