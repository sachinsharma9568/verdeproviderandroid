package com.example.aircraftprovider.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.PendingAction1Activity
import com.example.aircraftprovider.adapter.PendingActionAdapter.PendingActionViewHolder
import com.example.aircraftprovider.model.ListModel

class PendingActionAdapter(var context: Context, var listModels: List<ListModel>) :
    RecyclerView.Adapter<PendingActionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PendingActionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.upcoming_request_adapter_view, parent, false)
        return PendingActionViewHolder(view)
    }

    override fun onBindViewHolder(holder: PendingActionViewHolder, position: Int) {
        holder.date.text = listModels[position].title
        holder.month.text = listModels[position].name
        holder.constraintLayout.setOnClickListener {
            val intent = Intent(context, PendingAction1Activity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return listModels.size
    }

    inner class PendingActionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView
        var month: TextView
        var constraintLayout: ConstraintLayout

        init {
            date = itemView.findViewById(R.id.tv_date_dd)
            month = itemView.findViewById(R.id.tv_date_mm)
            constraintLayout = itemView.findViewById(R.id.constraintlayout)
        }
    }
}