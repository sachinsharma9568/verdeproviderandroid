package com.example.aircraftprovider.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.CrewMemberDetailsActivity
import com.example.aircraftprovider.adapter.ViewListingActivityAdapter.ViewListingViewHolder
import com.example.aircraftprovider.model.ListModel

class ViewListingActivityAdapter(var context: Context, var listModels: List<ListModel>) :
    RecyclerView.Adapter<ViewListingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewListingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.view_listing_adapter_view, parent, false)
        return ViewListingViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewListingViewHolder, position: Int) {
        holder.name.text = listModels[position].title
        holder.imgforward.setOnClickListener {
            context.startActivity(
                Intent(
                    context,
                    CrewMemberDetailsActivity::class.java
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return listModels.size
    }

    inner class ViewListingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var imgforward: ImageView

        init {
            name = itemView.findViewById(R.id.tv_membername)
            imgforward = itemView.findViewById(R.id.img_forward)
        }
    }
}