package com.example.aircraftprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.HomeFragmentHistoryAdapter.HistoryViewHolder
import com.example.aircraftprovider.model.ListModel

class HomeFragmentHistoryAdapter(var context: Context, var listModelhistry: List<ListModel>) :
    RecyclerView.Adapter<HistoryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.home_history_adapter_view, parent, false)
        return HistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.sname.text = listModelhistry[position].title
        holder.dname.text = listModelhistry[position].name
    }

    override fun getItemCount(): Int {
        return listModelhistry.size
    }

    inner class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var sname: TextView
        var dname: TextView

        init {
            sname = itemView.findViewById(R.id.tv_startingpointname)
            dname = itemView.findViewById(R.id.tv_destinationname)
        }
    }
}