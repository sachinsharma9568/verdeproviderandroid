package com.verde.aircraft.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetProfileResponse {

    @SerializedName("code")
    @Expose
    var code: Int? = null

    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: Data? = null

    class Data {
        @SerializedName("id")
        @Expose
        var id: Int? = null

        @SerializedName("account_type")
        @Expose
        var accountType: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("company_name")
        @Expose
        var companyName: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("user_type")
        @Expose
        var userType: Any? = null

        @SerializedName("device_type")
        @Expose
        var deviceType: String? = null

        @SerializedName("term_and_condition")
        @Expose
        var termAndCondition: String? = null

        @SerializedName("device_token")
        @Expose
        var deviceToken: String? = null

        @SerializedName("verification_token")
        @Expose
        var verificationToken: String? = null

        @SerializedName("account_status")
        @Expose
        var accountStatus: String? = null

        @SerializedName("email_verified_at")
        @Expose
        var emailVerifiedAt: String? = null

        @SerializedName("otp")
        @Expose
        var otp: String? = null

        @SerializedName("social_token")
        @Expose
        var socialToken: Any? = null

        @SerializedName("lat")
        @Expose
        var lat: Float? = null

        @SerializedName("long")
        @Expose
        var _long: Float? = null

        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
    }
}