package com.example.aircraftprovider.model.otpverification

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OtpVerificationResponse {
    @SerializedName("code")
    @Expose
    var code: Int? = null

    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: Data? = null
}