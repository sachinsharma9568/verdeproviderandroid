package com.example.aircraftprovider.model.forgotpassword

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForgotResponse {
    @SerializedName("code")
    @Expose
    var code: Int? = null

    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: String? = null
}