package com.example.aircraftprovider.model.verification

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Data {
    @SerializedName("data")
    @Expose
    var data: String? = null
}