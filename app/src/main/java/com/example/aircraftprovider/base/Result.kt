package com.user.angelfood.base

import java.lang.Exception

sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error<out T : Any>(val exception: String) : Result<T>()
    data class ApiError<out T : Any>(val exception: String) : Result<T>()
}