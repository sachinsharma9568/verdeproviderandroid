package com.user.angelfood.base

import android.util.Log
import com.user.angelfood.utils.AppConstants
import retrofit2.Response
import com.user.angelfood.utils.ApiException
import com.user.angelfood.base.Result

abstract class BaseRepository {
    fun <T : Any> safeApiCall(call: Response<T>): T {
        val result: Result<T> = safeApiResult(call)
        var data: T? = null

        Log.d("result", result.toString())
        when (result) {
            is Result.Success ->
                data = result.data

            is Result.Error -> {
                throw ApiException(result.exception)
            }

            is Result.ApiError -> {
                Log.d("API_ERROR", result.exception)
            }
        }
        return data!!
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> safeApiResult(call: Response<T>): Result<T> {
        return if (call.isSuccessful && call.code() == AppConstants.SUCCESS_CODE) {
            Log.d("API_RESPONSE", call.body().toString())
            Result.Success(call.body()!!)
        } else {
            Log.d("API_RESPONSE_ERROR", call.errorBody().toString())
            Result.Error("Something went wrong !!")
            //below code is for parsing error getting from backend
            //Result.Error(ErrorHandlerUtils.handleApiErrorResponse(response.errorBody()).error.message)
        }


    }
}