package com.user.angelfood.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.user.angelfood.activities.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseFragment : Fragment(), CoroutineScope {
    lateinit var mJob: Job
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main //To change initializer of created properties use File | Settings | File Templates.

    //val pref by lazy { PrefManager.getInstance(requireContext()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mJob = Job()
    }

    override fun onResume() {
        super.onResume()
        if (context is MainActivity) {
            {
                // (context as MainActivity) .hideKeyboard()
                //(context as MainActivity).closeDrawer()

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mJob.cancel()
    }


}