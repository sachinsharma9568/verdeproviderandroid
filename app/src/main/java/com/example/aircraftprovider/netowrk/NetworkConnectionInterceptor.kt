package com.user.angelfood.network.interceptor

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.user.angelfood.R
import com.user.angelfood.utils.NetworkUtils
import com.user.angelfood.utils.NoInternetException
import com.user.angelfood.utils.showToast
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor constructor(var context: Context) : Interceptor{

    override fun intercept(chain: Interceptor.Chain): Response {
        if(!NetworkUtils.isOnline(context)){

            //  context.startActivity(Intent(context,NoInternetActivity::class.java))
            if(context is AppCompatActivity){
                context.showToast("Activity")
            }
            if(context is Fragment){
                context.showToast("Fragment")

            }
            throw NoInternetException(message = context.getString(R.string.no_internet_connection))
        }
        return  chain.proceed(chain.request())
    }
}