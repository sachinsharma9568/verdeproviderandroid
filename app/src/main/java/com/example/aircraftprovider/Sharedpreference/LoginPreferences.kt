package com.example.aircraftprovider.Sharedpreference

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class LoginPreferences(private val context: Context?) {
    private var editor: SharedPreferences.Editor? = null
    private val stateId: String? = null
    private val user_id = "userid"
    private val usertoken = "usertoken"
    private val company_email = "email"
    private val company_name = "name"
    private val profile_image = "profile_image"
    private val allowlocation = "location"
    fun getmPreferences(): SharedPreferences? {
        return mPreferences
    }

    private fun setmPreferences(mPreferences: SharedPreferences) {
        this.mPreferences = mPreferences
    }

    var token: String?
        get() = mPreferences!!.getString(usertoken, "")
        set(token) {
            editor = mPreferences!!.edit()
            editor.putString(usertoken, token)
            editor.apply()
        }
    var location: String?
        get() = mPreferences!!.getString(allowlocation, "")
        set(token) {
            editor = mPreferences!!.edit()
            editor.putString(allowlocation, token)
            editor.apply()
        }
    var userId: String?
        get() = mPreferences!!.getString(user_id, "")
        set(user_id) {
            editor = mPreferences!!.edit()
            editor.putString(this.user_id, user_id)
            editor.apply()
        }
    var userName: String?
        get() = mPreferences!!.getString(company_name, "")
        set(name) {
            editor = mPreferences!!.edit()
            editor.putString(company_name, name)
            editor.apply()
        }
    var userEmail: String?
        get() = mPreferences!!.getString(company_email, "")
        set(email) {
            editor = mPreferences!!.edit()
            editor.putString(company_email, email)
            editor.apply()
        }
    var userProfile: String?
        get() = mPreferences!!.getString(profile_image, "")
        set(profile_image) {
            editor = mPreferences!!.edit()
            editor.putString(this.profile_image, profile_image)
            editor.apply()
        }

    companion object {
        private var preferences: LoginPreferences? = null
        private val mPreferences: SharedPreferences? = null
        fun getActiveInstance(context: Context?): LoginPreferences? {
            if (preferences == null) {
                preferences = LoginPreferences(context)
            }
            return preferences
        }

        fun deleteAllPreference() {
            mPreferences!!.edit().clear().apply()
        }
    }

    init {
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context))
    }
}