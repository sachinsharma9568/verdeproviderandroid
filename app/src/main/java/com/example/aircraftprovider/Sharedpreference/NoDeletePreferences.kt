package com.example.aircraftprovider.Sharedpreference

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class NoDeletePreferences(private val context: Context?) {
    private var editor: SharedPreferences.Editor? = null
    private val stateId: String? = null
    var firsttimelogin = "firsttimelogin"
        get() = mPreferences1!!.getString(field, "")!!
        set(firsttimelogin) {
            editor = mPreferences1!!.edit()
            editor.putString(this.firsttimelogin, firsttimelogin)
            editor.apply()
        }

    fun getmPreferences(): SharedPreferences? {
        return mPreferences1
    }

    private fun setmPreferences(mPreferences: SharedPreferences) {
        mPreferences1 = mPreferences
    }

    companion object {
        private var preferences: NoDeletePreferences? = null
        private var mPreferences1: SharedPreferences? = null
        fun getActiveInstance(context: Context?): NoDeletePreferences? {
            if (preferences == null) {
                preferences = NoDeletePreferences(context)
            }
            return preferences
        }
    }

    init {
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context))
    }
}