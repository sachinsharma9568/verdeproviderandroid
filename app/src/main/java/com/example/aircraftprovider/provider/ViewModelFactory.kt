package com.user.angelfood.provider

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.kodein.di.DKodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance
import org.kodein.di.generic.instanceOrNull

class ViewModelFactory (private val injector: DKodein) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return injector.instanceOrNull<ViewModel>(tag = modelClass.simpleName) as T?
            ?: modelClass.newInstance()
    }
}

//if you want save data when fragment being replaced by another fragment then pass
//@activity reference in ViewModelProviders

    inline fun <reified VM : ViewModel, T> T.viewModel(): Lazy<VM> where T : KodeinAware, T : Fragment{
        return lazy { ViewModelProvider(activity!!, direct.instance()).get(VM::class.java) }
    }

    //if you don't want then pass fragment reference
    inline fun <reified VM : ViewModel, T> T.fragmentViewModel(): Lazy<VM> where T : KodeinAware, T : Fragment{
        return lazy { ViewModelProvider(this, direct.instance()).get(VM::class.java) }
    }

    inline fun <reified VM : ViewModel, T> T.activityViewModel() : Lazy<VM> where T: AppCompatActivity, T :KodeinAware{
        return lazy { ViewModelProvider(this,direct.instance()).get(VM::class.java) }
    }
