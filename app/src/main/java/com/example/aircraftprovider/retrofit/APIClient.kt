package com.example.aircraftprovider.retrofit

import com.example.aircraftprovider.util.AppConstant
import com.google.gson.GsonBuilder
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIClient {
    private var retrofit: Retrofit? = null
    fun retrofit(): Retrofit? {
        val httpClient = Builder()
        val gson = GsonBuilder()
            .setLenient()
            .create()
        httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(AppConstant.BASE_URL)
                .client(httpClient.build())
                .build()
        }
        return retrofit
    }
}