package com.example.aircraftprovider.retrofit

import com.example.aircraftprovider.model.forgotpassword.ForgotResponse
import com.example.aircraftprovider.model.login.LoginResponse
import com.example.aircraftprovider.model.otpverification.OtpVerificationResponse
import com.example.aircraftprovider.model.resetpassword.ResetPasswordResponse
import com.example.aircraftprovider.model.signup.SignUpResponse
import com.example.aircraftprovider.model.verification.VerificationResponse
import com.example.aircraftprovider.util.AppConstant
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface APIInterfacesService {
    @FormUrlEncoded
    @POST(AppConstant.API_SIGN_UP)
    fun getRegister(
        @Field("account_type") account_type: String?,
        @Field("name") username: String?,
        @Field("email") email: String?,
        @Field("password") password: String?,
        @Field("confirm_password") password_confirmation: String?,
        @Field("term_and_condition") term_and_condition: String?,
        @Field("device_token") device_token: String?,
        @Field("device_type") device_type: String?,
        @Field("lat") lat: String?,
        @Field("long") var_long: String?,
        @Field("mobile") mobile: String?,
        @Field("company_name") company_name: String?
    ): Call<SignUpResponse?>?

    @FormUrlEncoded
    @POST(AppConstant.API_Email_Verification)
    fun getVerification(@Field("email") email: String?): Call<VerificationResponse?>?

    @FormUrlEncoded
    @POST(AppConstant.API_LOGIN)
    fun getLogin(
        @Field("account_type") account_type: String?,
        @Field("email") email: String?,
        @Field("password") password: String?,
        @Field("device_token") device_token: String?,
        @Field("device_type") device_type: String?,
        @Field("lat") lat: String?,
        @Field("long") var_long: String?
    ): Call<LoginResponse?>?

    @FormUrlEncoded
    @POST(AppConstant.API_FORGOT_PASSWORD)
    fun getForgotPassword(
        @Field("email") email: String?,
        @Field("account_type") account_type: String?
    ): Call<ForgotResponse?>?

    @POST(AppConstant.API_VERIFY_OTP)
    @FormUrlEncoded
    fun getVerifyOTP(
        @Field("email") email: String?,
        @Field("otp") otp: String?
    ): Call<OtpVerificationResponse?>?

    @FormUrlEncoded
    @POST(AppConstant.API_RESET_PASSWORD)
    fun getResetPassword(
        @Field("email") email: String?,
        @Field("password") password: String?,
        @Field("confirm_password") confirm_password: String?
    ): Call<ResetPasswordResponse?>?
}