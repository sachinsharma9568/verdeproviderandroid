package com.example.aircraftprovider.viewHolder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aircraftprovider.model.login.LoginResponse
import com.example.aircraftprovider.retrofit.APIClient.retrofit
import com.example.aircraftprovider.retrofit.APIInterfacesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel : ViewModel() {
    var errorMsg = MutableLiveData<String?>()
    var loadingstatus = MutableLiveData<Boolean>()
    var mResponse: MutableLiveData<LoginResponse?>? = null
    val errorMessage: LiveData<String?>
        get() = errorMsg
    val loadingStatus: LiveData<Boolean>
        get() = loadingstatus
    val data: MutableLiveData<LoginResponse?>
        get() {
            if (mResponse == null) {
                mResponse = MutableLiveData()
            }
            return mResponse!!
        }

    fun setData(
        account_type: String?,
        email: String?,
        password: String?,
        device_token: String?,
        device_type: String?,
        lat: String?,
        var_long: String?
    ) {
        val apiInterfacesService = retrofit()!!.create(
            APIInterfacesService::class.java
        )
        val mcall = apiInterfacesService.getLogin(
            account_type,
            email,
            password,
            device_token,
            device_type,
            lat,
            var_long
        )
        loadingstatus.postValue(true)
        mcall!!.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.isSuccessful && response.code() == 200) {
                    Log.d("onSuccess", response.body().toString())
                    Log.d("onSuccess", response.body()!!.code.toString())
                    if (response.body()!!.code == 200) {
                        loadingstatus.postValue(false)
                        mResponse!!.postValue(response.body())
                    } else {
                        loadingstatus.postValue(false)
                        errorMsg.postValue(response.body()!!.message)
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingstatus.postValue(false)
            }
        })
    }
}