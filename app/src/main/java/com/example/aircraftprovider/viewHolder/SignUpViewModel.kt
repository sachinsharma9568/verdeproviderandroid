package com.example.aircraftprovider.viewHolder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aircraftprovider.model.signup.SignUpResponse
import com.example.aircraftprovider.retrofit.APIClient.retrofit
import com.example.aircraftprovider.retrofit.APIInterfacesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpViewModel : ViewModel() {
    var errorMsg: MutableLiveData<String?> = MutableLiveData<Any?>()
    var loadingStatus: MutableLiveData<Boolean?> = MutableLiveData<Any?>()
    var registerResponse: MutableLiveData<SignUpResponse?>? = null
    fun getLoadingStatus(): LiveData<Boolean?> {
        return loadingStatus
    }

    val errorMessage: LiveData<String?>
        get() = errorMsg
    val userRegister: MutableLiveData<SignUpResponse?>
        get() {
            if (registerResponse == null) {
                registerResponse = MutableLiveData()
            }
            return registerResponse!!
        }

    fun setUserSignUp(
        account_type: String?,
        username: String?,
        email: String?,
        password: String?,
        password_confirmation: String?,
        term_and_condition: String?,
        device_token: String?,
        device_type: String?,
        lat: String?,
        var_long: String?,
        mobile: String?,
        company_name: String?
    ) {
        val apiInterfacesService = retrofit()!!.create(
            APIInterfacesService::class.java
        )
        val mCall = apiInterfacesService.getRegister(
            account_type,
            username,
            email,
            password,
            password_confirmation,
            term_and_condition,
            device_token,
            device_type,
            lat,
            var_long,
            mobile,
            company_name
        )
        loadingStatus.postValue(true)
        mCall!!.enqueue(object : Callback<SignUpResponse> {
            override fun onResponse(
                call: Call<SignUpResponse>,
                response: Response<SignUpResponse>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    Log.d("onSuccess", response.body().toString())
                    Log.d("onSuccess", response.body()!!.code.toString())
                    if (response.body()!!.code == 200) {
                        loadingStatus.postValue(false)
                        registerResponse!!.postValue(response.body())
                    } else {
                        loadingStatus.postValue(false)
                        errorMsg.postValue(response.body().getMessage())
                    }
                } else if (response.body()!!.code == 400) {
                    loadingStatus.postValue(false)
                    errorMsg.postValue(response.body()!!.message.toString())
                    Log.e("onerror", "email already exist.")
                }
            }

            override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue("The email has already been taken.")
                loadingStatus.postValue(false)
            }
        })
    }
}