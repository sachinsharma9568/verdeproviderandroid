package com.example.aircraftprovider.viewHolder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aircraftprovider.model.resetpassword.ResetPasswordResponse
import com.example.aircraftprovider.retrofit.APIClient.retrofit
import com.example.aircraftprovider.retrofit.APIInterfacesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPasswordViewModel : ViewModel() {
    var errorMsg: MutableLiveData<String?> = MutableLiveData<Any?>()
    var loadingStatusReset: MutableLiveData<Boolean?> = MutableLiveData<Any?>()
    var mResponse: MutableLiveData<ResetPasswordResponse?>? = null
    fun getLoadingStatusReset(): LiveData<Boolean?> {
        return loadingStatusReset
    }

    val errorMessage: LiveData<String?>
        get() = errorMsg
    val data: MutableLiveData<ResetPasswordResponse?>
        get() {
            if (mResponse == null) {
                mResponse = MutableLiveData()
            }
            return mResponse!!
        }

    fun setData(email: String?, password: String?, confirmpassword: String?) {
        val apiInterfacesService = retrofit()!!.create(
            APIInterfacesService::class.java
        )
        val mCall = apiInterfacesService.getResetPassword(email, password, confirmpassword)
        loadingStatusReset.postValue(true)
        mCall!!.enqueue(object : Callback<ResetPasswordResponse> {
            override fun onResponse(
                call: Call<ResetPasswordResponse>,
                response: Response<ResetPasswordResponse>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    try {
                        Log.d("onSuccess=====", response.body()!!.message!!)
                        if (response.body()!!.code == 200) {
                            loadingStatusReset.postValue(false)
                            mResponse!!.postValue(response.body())
                        } else {
                            loadingStatusReset.postValue(false)
                            errorMsg.postValue(response.body()!!.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    loadingStatusReset.postValue(false)
                    errorMsg.postValue("Something went wrong")
                }
            }

            override fun onFailure(call: Call<ResetPasswordResponse>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatusReset.postValue(false)
            }
        })
    }
}