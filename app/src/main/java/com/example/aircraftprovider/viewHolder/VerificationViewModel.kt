package com.example.aircraftprovider.viewHolder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aircraftprovider.model.verification.VerificationResponse
import com.example.aircraftprovider.retrofit.APIClient.retrofit
import com.example.aircraftprovider.retrofit.APIInterfacesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerificationViewModel : ViewModel() {
    var errorMsg: MutableLiveData<String?> = MutableLiveData<Any?>()
    var loadingStatus: MutableLiveData<Boolean?> = MutableLiveData<Any?>()
    var verificationResponse: MutableLiveData<VerificationResponse?>? = null
    fun getLoadingStatus(): LiveData<Boolean?> {
        return loadingStatus
    }

    val errorMessage: LiveData<String?>
        get() = errorMsg

    fun getVerificationResponse(): MutableLiveData<VerificationResponse?> {
        if (verificationResponse == null) {
            verificationResponse = MutableLiveData()
        }
        return verificationResponse!!
    }

    fun setUserVerification(email: String?) {
        val apiInterfacesService = retrofit()!!.create(
            APIInterfacesService::class.java
        )
        val mCall = apiInterfacesService.getVerification(email)
        loadingStatus.postValue(true)
        mCall!!.enqueue(object : Callback<VerificationResponse> {
            override fun onResponse(
                call: Call<VerificationResponse>,
                response: Response<VerificationResponse>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    Log.d("onSuccess", response.body().toString())
                    Log.d("onSuccess", response.body()!!.code.toString())
                    if (response.body()!!.code == 200) {
                        loadingStatus.postValue(false)
                        verificationResponse!!.postValue(response.body())
                    } else {
                        loadingStatus.postValue(false)
                        errorMsg.postValue(response.body()!!.message)
                    }
                } /*else if (response.code() == 403) {
                    Log.d("onSuccess", "User does not exit.");
                    loadingStatus.postValue(false);
                    errorMsg.postValue("Something went wrong");
                }*/
            }

            override fun onFailure(call: Call<VerificationResponse>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatus.postValue(false)
            }
        })
    }
}