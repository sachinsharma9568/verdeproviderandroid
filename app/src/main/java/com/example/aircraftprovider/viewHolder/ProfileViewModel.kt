package com.verde.aircraft.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.verde.aircraft.model.GetProfileResponse
import com.verde.aircraft.utils.APIClient
import com.verde.aircraft.utils.ApiInterface
import com.verde.aircraft.utils.getRequestBody
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileViewModel : ViewModel() {

    var errorMsg: MutableLiveData<String?> = MutableLiveData<String?>()
    var loadingStatus: MutableLiveData<Boolean?> = MutableLiveData<Boolean?>()
    var mResponse: MutableLiveData<GetProfileResponse?>? = null

    fun getLoadingStatus(): LiveData<Boolean?> {
        return loadingStatus
    }

    val errorMessage: LiveData<String?>
        get() = errorMsg

    val getProfileLiveData: MutableLiveData<GetProfileResponse?>
        get() {
            if (mResponse == null) {
                mResponse = MutableLiveData()
            }
            return mResponse!!
        }

    fun getProfileData(authToken: String?) {
        val apiInterfacesService = APIClient.retrofit()!!.create(
            ApiInterface::class.java
        )
        val mCall = apiInterfacesService.getProfileData("Bearer "+authToken)
        loadingStatus.postValue(true)
        mCall.enqueue(object : Callback<GetProfileResponse?> {
            override fun onResponse(
                call: Call<GetProfileResponse?>,
                response: Response<GetProfileResponse?>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    try {
                        Log.d("onSuccess=====", response.body()!!.message!!)
                        if (response.body()!!.code == 200) {
                            loadingStatus.postValue(false)
                            mResponse!!.postValue(response.body())
                        } else {
                            loadingStatus.postValue(false)
                            errorMsg.postValue(response.body()!!.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    Log.d("Code", "" + response.code())
                    loadingStatus.postValue(false)
                    errorMsg.postValue("Something went wrong")
                }
            }

            override fun onFailure(call: Call<GetProfileResponse?>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatus.postValue(false)
            }
        })
    }

    fun changeNameApi(authToken: String, name: String) {
        val apiInterfacesService = APIClient.retrofit()!!.create(
            ApiInterface::class.java
        )
        val mCall = apiInterfacesService.getChangeName("Bearer "+authToken, name)
        loadingStatus.postValue(true)
        mCall.enqueue(object : Callback<GetProfileResponse?> {
            override fun onResponse(
                call: Call<GetProfileResponse?>,
                response: Response<GetProfileResponse?>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    try {
                        Log.d("onSuccess=====", response.body()!!.message!!)
                        if (response.body()!!.code == 200) {
                            loadingStatus.postValue(false)
                            mResponse!!.postValue(response.body())
                        } else {
                            loadingStatus.postValue(false)
                            errorMsg.postValue(response.body()!!.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    Log.d("Code", "" + response.code())
                    loadingStatus.postValue(false)
                    errorMsg.postValue("Something went wrong")
                }
            }

            override fun onFailure(call: Call<GetProfileResponse?>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatus.postValue(false)
            }
        })
    }

    fun queryUpdateProfile(authToken: String, uploadedImageFile: File?) {
        val apiInterfacesService = APIClient.retrofit()!!.create(
            ApiInterface::class.java
        )
        val image: MultipartBody.Part
            val requestFile = uploadedImageFile.getRequestBody()
            image = MultipartBody.Part.createFormData(
                "profile_img",
                uploadedImageFile!!.name,
                requestFile
            )

        val mCall = apiInterfacesService.uploadProfilePic("Bearer "+authToken,image)
        loadingStatus.postValue(true)
        mCall.enqueue(object : Callback<GetProfileResponse?> {
            override fun onResponse(
                call: Call<GetProfileResponse?>,
                response: Response<GetProfileResponse?>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    try {
                        Log.d("onSuccess=====", response.body()!!.message!!)
                        if (response.body()!!.code == 200) {
                            loadingStatus.postValue(false)
                            mResponse!!.postValue(response.body())
                        } else {
                            loadingStatus.postValue(false)
                            errorMsg.postValue(response.body()!!.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    Log.d("Code", "" + response.code())
                    loadingStatus.postValue(false)
                    errorMsg.postValue("Something went wrong")
                }
            }

            override fun onFailure(call: Call<GetProfileResponse?>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatus.postValue(false)
            }
        })
    }

}