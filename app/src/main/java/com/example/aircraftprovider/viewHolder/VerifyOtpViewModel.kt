package com.example.aircraftprovider.viewHolder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aircraftprovider.model.otpverification.OtpVerificationResponse
import com.example.aircraftprovider.retrofit.APIClient.retrofit
import com.example.aircraftprovider.retrofit.APIInterfacesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyOtpViewModel : ViewModel() {
    var errorMsg: MutableLiveData<String?> = MutableLiveData<Any?>()
    var loadingStatus: MutableLiveData<Boolean?> = MutableLiveData<Any?>()
    var mResponse: MutableLiveData<OtpVerificationResponse?>? = null
    fun getLoadingStatus(): LiveData<Boolean?> {
        return loadingStatus
    }

    val errorMessage: LiveData<String?>
        get() = errorMsg
    val verifyOtp: MutableLiveData<OtpVerificationResponse?>
        get() {
            if (mResponse == null) {
                mResponse = MutableLiveData()
            }
            return mResponse!!
        }

    fun setVerifyOtp(email: String?, otp: String?) {
        val apiInterfacesService = retrofit()!!.create(
            APIInterfacesService::class.java
        )
        val mCall = apiInterfacesService.getVerifyOTP(email, otp)
        loadingStatus.postValue(true)
        mCall!!.enqueue(object : Callback<OtpVerificationResponse> {
            override fun onResponse(
                call: Call<OtpVerificationResponse>,
                response: Response<OtpVerificationResponse>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    try {
                        Log.d("onSuccess=====", response.body()!!.message!!)
                        if (response.body()!!.code == 200) {
                            loadingStatus.postValue(false)
                            mResponse!!.postValue(response.body())
                        } else {
                            loadingStatus.postValue(false)
                            errorMsg.postValue(response.body()!!.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    Log.d("Code", "" + response.code())
                    loadingStatus.postValue(false)
                    errorMsg.postValue("Something went wrong")
                }
            }

            override fun onFailure(call: Call<OtpVerificationResponse>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatus.postValue(false)
            }
        })
    }
}