package com.example.aircraftprovider.viewHolder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aircraftprovider.model.forgotpassword.ForgotResponse
import com.example.aircraftprovider.retrofit.APIClient.retrofit
import com.example.aircraftprovider.retrofit.APIInterfacesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgetPasswordViewModel : ViewModel() {
    var errorMsg: MutableLiveData<String?> = MutableLiveData<Any?>()
    var loadingStatus: MutableLiveData<Boolean?> = MutableLiveData<Any?>()
    var mResponse: MutableLiveData<ForgotResponse?>? = null
    fun getLoadingStatus(): LiveData<Boolean?> {
        return loadingStatus
    }

    val errorMessage: LiveData<String?>
        get() = errorMsg
    val data: MutableLiveData<ForgotResponse?>
        get() {
            if (mResponse == null) {
                mResponse = MutableLiveData()
            }
            return mResponse!!
        }

    fun setData(email: String?, account_type: String?) {
        val apiInterfacesService = retrofit()!!.create(
            APIInterfacesService::class.java
        )
        val mCall = apiInterfacesService.getForgotPassword(email, account_type)
        loadingStatus.postValue(true)
        mCall!!.enqueue(object : Callback<ForgotResponse> {
            override fun onResponse(
                call: Call<ForgotResponse>,
                response: Response<ForgotResponse>
            ) {
                if (response.isSuccessful && response.code() == 200) {
                    try {
                        Log.d("onSuccess=====", response.body()!!.message!!)
                        if (response.body()!!.code == 200) {
                            loadingStatus.postValue(false)
                            mResponse!!.postValue(response.body())
                        } else {
                            loadingStatus.postValue(false)
                            errorMsg.postValue(response.body()!!.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    loadingStatus.postValue(false)
                    errorMsg.postValue("Something went wrong")
                }
            }

            override fun onFailure(call: Call<ForgotResponse>, t: Throwable) {
                Log.d("onFailure", t.message!!)
                errorMsg.postValue(t.message)
                loadingStatus.postValue(false)
            }
        })
    }
}