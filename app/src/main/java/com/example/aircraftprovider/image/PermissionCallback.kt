package com.diamondbrows.ui.fragments.settings

interface PermissionCallback {
    fun allPermissionGranted(isGranted : Boolean = false)
}