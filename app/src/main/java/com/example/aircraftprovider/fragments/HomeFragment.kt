package com.example.aircraftprovider.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.MainActivity
import com.example.aircraftprovider.activities.UpcomingRequestActivity
import com.example.aircraftprovider.adapter.HomeFragmentHistoryAdapter
import com.example.aircraftprovider.adapter.HomeFragmentUpcomingAdapter
import com.example.aircraftprovider.databinding.FragmentHomeBinding
import com.example.aircraftprovider.model.ListModel
import java.util.*

class HomeFragment : Fragment() {
    var binding: FragmentHomeBinding? = null
    var listModels: MutableList<ListModel> = ArrayList()
    var listModelshistory: MutableList<ListModel> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        val root = binding.getRoot()
        init()
        listener()
        return root
    }

    private fun listener() {
        binding!!.tvSeeAll.setOnClickListener {
            startActivity(
                Intent(
                    activity, UpcomingRequestActivity::class.java
                )
            )
        }
    }

    private fun init() {
        binding!!.rvupcoming.layoutManager = GridLayoutManager(context, 2)
        binding!!.rvupcoming.adapter = HomeFragmentUpcomingAdapter(context, listModels)
        binding!!.rvopportunities.layoutManager = GridLayoutManager(context, 2)
        binding!!.rvopportunities.adapter = HomeFragmentUpcomingAdapter(context, listModels)
        binding!!.rvhistory.layoutManager = LinearLayoutManager(context)
        binding!!.rvhistory.adapter = HomeFragmentHistoryAdapter(context, listModelshistory)
        listModels.clear()
        listModels.add(ListModel("12", "April"))
        listModels.add(ListModel("6", "May"))
        listModelshistory.clear()
        listModelshistory.add(ListModel("Newyork", "Dubai"))
        listModelshistory.add(ListModel("Delhi", "Goa"))
        (requireActivity() as MainActivity).updateToolbarTitle(resources.getString(R.string.home))
        (requireActivity() as MainActivity).setBottomBar(0)
    }
}