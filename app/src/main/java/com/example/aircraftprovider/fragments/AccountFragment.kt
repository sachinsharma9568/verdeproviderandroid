package com.example.aircraftprovider.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.MainActivity

class AccountFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (requireActivity() as MainActivity).updateToolbarTitle(resources.getString(R.string.account))
        (requireActivity() as MainActivity).setBottomBar(3)
        return inflater.inflate(R.layout.fragment_account, container, false)
    }
}