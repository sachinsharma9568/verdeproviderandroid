package com.example.aircraftprovider.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.*
import com.example.aircraftprovider.databinding.FragmentViewListingBinding

class ViewListingFragment : Fragment() {
    var binding: FragmentViewListingBinding? = null
    var clickItems: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_view_listing, container, false)
        val root = binding!!.getRoot()
        init()
        listener()
        return root
    }

    private fun listener() {
        binding!!.addaircraftConstraints.setOnClickListener {
            clickItems = "1"
            setBgColor(clickItems)
        }
        binding!!.addPilotConstraint.setOnClickListener {
            clickItems = "2"
            setBgColor(clickItems)
        }
        binding!!.viewcrewConstraints.setOnClickListener {
            clickItems = "3"
            setBgColor(clickItems)
        }

        binding!!.btnsubmit.setOnClickListener {
            if (clickItems.equals("1")) {
                startActivity(
                    Intent(
                        activity, ViewAircraftListing::class.java
                    )
                )
            } else if (clickItems.equals("2")) {
                startActivity(
                    Intent(
                        activity,
                        ViewPilotListing::class.java
                    )
                )
            } else if (clickItems.equals("3")) {
                startActivity(
                    Intent(
                        activity,
                        ViewListingActivity::class.java
                    )
                )
            }
        }


    }

    private fun setBgColor(clickItems: String) {
        if (clickItems.equals("1")) {
            binding!!.addaircraftConstraints.setBackgroundResource(R.drawable.ic_button_green)
            binding!!.tvMybooking.setTextColor(Color.parseColor("#FFFFFF"))
            binding!!.ivPiloticon.setBackgroundResource(R.drawable.ic_circle_grey)
            binding!!.ivcalender.setBackgroundResource(R.drawable.ic_small_white_circle)
            binding!!.ivmultipleuser.setBackgroundResource(R.drawable.ic_circle_grey)

            binding!!.addPilotConstraint.setBackgroundResource(R.drawable.rectangle_white_background)
            binding!!.tvAddpilot.setTextColor(Color.parseColor("#444444"))
        //    binding!!.viewcrew_constraints.setBackgroundResource(R.drawable.rectangle_white_background)
        //    binding!!.tvAddCrew.setTextColor(Color.parseColor("#444444"))
        }
        else if (clickItems.equals("2")) {
            binding!!.addPilotConstraint.setBackgroundResource(R.drawable.ic_button_green)
            binding!!.tvAddpilot.setTextColor(Color.parseColor("#FFFFFF"))
            binding!!.ivPiloticon.setBackgroundResource(R.drawable.ic_small_white_circle)
            binding!!.ivcalender.setBackgroundResource(R.drawable.ic_circle_grey)
            binding!!.ivmultipleuser.setBackgroundResource(R.drawable.ic_circle_grey)


            binding!!.addaircraftConstraints.setBackgroundResource(R.drawable.rectangle_white_background)
            binding!!.tvMybooking.setTextColor(Color.parseColor("#444444"))
         //   binding!!.addcrewConstraints.setBackgroundResource(R.drawable.rectangle_white_background)
         //   binding!!.tvAddCrew.setTextColor(Color.parseColor("#444444"))

        } else if (clickItems.equals("3")) {
        //    binding!!.addcrewConstraints.setBackgroundResource(R.drawable.ic_button_green)
        //    binding!!.tvAddCrew.setTextColor(Color.parseColor("#FFFFFF"))
            binding!!.ivmultipleuser.setBackgroundResource(R.drawable.ic_small_white_circle)
            binding!!.ivPiloticon.setBackgroundResource(R.drawable.ic_circle_grey)
            binding!!.ivcalender.setBackgroundResource(R.drawable.ic_circle_grey)

            binding!!.addPilotConstraint.setBackgroundResource(R.drawable.rectangle_white_background)
            binding!!.tvAddpilot.setTextColor(Color.parseColor("#444444"))
            binding!!.addaircraftConstraints.setBackgroundResource(R.drawable.rectangle_white_background)
            binding!!.tvMybooking.setTextColor(Color.parseColor("#444444"))

        }
    }


    private fun init() {
        (requireActivity() as MainActivity).updateToolbarTitle(resources.getString(R.string.view_listing))
        (requireActivity() as MainActivity).setBottomBar(1)
    }
}