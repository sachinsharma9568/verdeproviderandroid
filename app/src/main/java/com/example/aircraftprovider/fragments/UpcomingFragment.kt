package com.example.aircraftprovider.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.PendingActionAdapter
import com.example.aircraftprovider.databinding.FragmentUpcomingBinding
import com.example.aircraftprovider.model.ListModel
import java.util.*

class UpcomingFragment : Fragment() {
    var binding: FragmentUpcomingBinding? = null
    var listModels: MutableList<ListModel> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_upcoming, container, false)
        val view = binding.getRoot()
        init()
        listener()
        return view
    }

    private fun init() {
        binding!!.rvUpcomingrequest.layoutManager = LinearLayoutManager(activity)
        binding!!.rvUpcomingrequest.adapter = PendingActionAdapter(activity, listModels)
        listModels.clear()
        listModels.add(ListModel("06", "May"))
        listModels.add(ListModel("23", "June"))
        listModels.add(ListModel("10", "July"))
    }

    private fun listener() {}
}