package com.example.aircraftprovider.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.AddAircraftActivity
import com.example.aircraftprovider.activities.AddCrewMemberActivity
import com.example.aircraftprovider.activities.AddPilotActivity
import com.example.aircraftprovider.activities.MainActivity
import com.example.aircraftprovider.databinding.FragmentAddNewBinding

class AddNewFragment : Fragment() {
    var binding: FragmentAddNewBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_new, container, false)
        val root = binding.getRoot()
        init()
        listener()
        return root
    }

    private fun listener() {
        binding!!.addaircraftConstraints.setOnClickListener {
            startActivity(
                Intent(
                    activity, AddAircraftActivity::class.java
                )
            )
        }
        binding!!.addPilotConstraint.setOnClickListener {
            startActivity(
                Intent(
                    activity,
                    AddPilotActivity::class.java
                )
            )
        }
        binding!!.addcrewConstraints.setOnClickListener {
            startActivity(
                Intent(
                    activity,
                    AddCrewMemberActivity::class.java
                )
            )
        }
    }

    private fun init() {
        (requireActivity() as MainActivity).updateToolbarTitle(resources.getString(R.string.add_new))
        (requireActivity() as MainActivity).setBottomBar(2)
    }
}