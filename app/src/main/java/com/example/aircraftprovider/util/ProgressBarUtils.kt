package com.example.aircraftprovider.util

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.WindowManager.BadTokenException
import android.widget.LinearLayout
import com.example.aircraftprovider.R

class ProgressBarUtils {
    val isLoading: Boolean
        get() = dialogBuilder != null && dialogBuilder!!.isShowing

    companion object {
        var dialogBuilder: Dialog? = null
        fun showProgressDialog(context: Context) {
            if (dialogBuilder != null) dialogBuilder!!.dismiss()
            dialogBuilder = Dialog(context, R.style.MaterialSearch)
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.progress_dialog_layout, null)
            dialogBuilder!!.setContentView(dialogView)
            dialogBuilder!!.setCancelable(false)
            dialogBuilder!!.setCanceledOnTouchOutside(false)
            dialogBuilder!!.window!!
                .setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
            try {
                dialogBuilder!!.show()
            } catch (e: BadTokenException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun hideProgressDialog() {
            try {
                if (dialogBuilder != null) dialogBuilder!!.dismiss()
            } catch (e: BadTokenException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}