package com.verde.aircraft.utils

import android.app.AlertDialog
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.text.*
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import com.verde.aircraft.R
import java.io.File
import java.lang.Exception


//extension function + inline function
//action function invoke itself
fun setSpanString(
    text: String,
    start: Int,
    end: Int,
    color: Int,
    action: (() -> Unit)? = null
): Spannable {
    val ssb = SpannableStringBuilder(text)
    val fcsRed = ForegroundColorSpan(color)
    val clickableSpan = object : ClickableSpan() {
        override fun onClick(textView: View) {
            action?.invoke()
        }

        override fun updateDrawState(drawState: TextPaint) {
            super.updateDrawState(drawState)
            drawState.isUnderlineText = false
        }
    }
    ssb.setSpan(clickableSpan, start, end, 0)
    ssb.setSpan(fcsRed, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    return ssb
}


fun TextView.setColouredSpan(word: String, color: Int, action: (() -> Unit)? = null) {
    val clickableSpan = object : ClickableSpan() {
        override fun onClick(textView: View) {
            action?.invoke()
        }

        override fun updateDrawState(drawState: TextPaint) {
            super.updateDrawState(drawState)
            drawState.isUnderlineText = false
        }
    }
    val spannableString = SpannableString(text)
    val start = text.indexOf(word)
    val end = text.indexOf(word) + word.length
    try {
        spannableString.setSpan(clickableSpan, start, end, 0)
        spannableString.setSpan(
            ForegroundColorSpan(color),
            start,
            end,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        text = spannableString
    } catch (e: IndexOutOfBoundsException) {
        println("'$word' was not not found in TextView text")
    }
}


//show toast ext. function
fun Context.showToast(toastString: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this@showToast, toastString, duration).show()
}


var isClicked = false

//show and hide ext. function
fun EditText.showAndHidePassword() {
    if (isClicked) {
        this.transformationMethod = PasswordTransformationMethod.getInstance();
        isClicked = false
    } else {
        this.transformationMethod = HideReturnsTransformationMethod.getInstance();
        isClicked = true
    }
    this.setSelection(this.text.length)
}

//request body for file ext. function
/*fun <T> T?.getRequestBody(): RequestBody {
    val result: T? = this
    if (result is File) {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), (result as File))
    } else if (result is String) {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), (result as String))
    }
    throw Exception("Invalid Format !! Pass String and File Only")
}*/
/*//get order status
fun Int.getOrderStatus(): String {
    if (this == AppConstants.STATUS_NEW)
        return "NEW"
    if (this == AppConstants.STATUS_ACCEPTED)
        return "ACCEPTED"
    if (this == AppConstants.STATUS_CANCELED)
        return "CANCELLED"
    if (this == AppConstants.STATUS_OUT_FOR_DELIVERY)
        return "OUT FOR DELIVERY"
    if (this == AppConstants.STATUS_DELIVERED)
        return "DELIVERED"
    if (this == AppConstants.STATUS_REFUND)
        return "REFUND"
    return "-"
}*/



//delete dialog
fun Context.showRemoveAlert(message: String, action: (() -> Unit)) {
    // build alert dialog
    val dialogBuilder = AlertDialog.Builder(this/* R.style.my_dialog_theme*/)
    // set message of alert dialog
    dialogBuilder.setMessage(message)
        // positive button text and action
        .setPositiveButton("Proceed") { _, _ ->
            action?.invoke()
        }
        // negative button text and action
        .setNegativeButton("Cancel") { dialog, _ ->
            dialog.cancel()
        }
    // create dialog box
    val alert = dialogBuilder.create()
    // show alert dialog
    alert.show()
}


//change rating bar color
fun RatingBar.changeRatingBar(mcontext: Context) {
    val stars = this.progressDrawable as LayerDrawable
    // for empty stars
    // stars.getDrawable(0).setColorFilter(ContextCompat.getColor(mcontext,R.color.colorRating), PorterDuff.Mode.SRC_ATOP)
    // for half filled stars
    stars.getDrawable(1).setColorFilter(
        ContextCompat.getColor(mcontext, R.color.black),
        PorterDuff.Mode.SRC_ATOP
    )
    // for filled stars
    stars.getDrawable(2).setColorFilter(
        ContextCompat.getColor(mcontext, R.color.blue),
        PorterDuff.Mode.SRC_ATOP
    )
}