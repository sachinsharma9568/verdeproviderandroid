package com.example.aircraftprovider.util

object AppConstant {
    const val BASE_URL = "https://webmobril.org/aircraft_booking/"
    const val GetDeviceType = "1"
    const val Account_Type = "2"
    const val Term_Condition = "1"
    const val API_SIGN_UP = "api/register"
    const val API_Email_Verification = "api/email_verified_user"
    const val API_LOGIN = "api/login"
    const val API_FORGOT_PASSWORD = "api/send_otp"
    const val API_VERIFY_OTP = "api/verify_opt"
    const val API_RESET_PASSWORD = "api/reset_password"
}