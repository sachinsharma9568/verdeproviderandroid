package com.verde.aircraft.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class PrefManager constructor(context: Context?) {

    private val sharedPreferences: SharedPreferences
    private var editor: SharedPreferences.Editor? = null

    init {
        val prefsFile = context?.packageName
        sharedPreferences = context!!.getSharedPreferences(prefsFile, Context.MODE_PRIVATE)
        sharedPreferences.edit().apply()
    }



    fun deletePreference(key: String) {
        if (sharedPreferences.contains(key)) {
            sharedPreferences.edit().remove(key).apply()
        }
    }

    //don't delete device token, device token will be restored at the time of
    //login
    fun deleteAllPreference() {
        try {
           // val deviceToken = getPreference<String>(AppConstants.DEVICE_TOKEN)
            sharedPreferences.edit().clear().apply()
            //savePreference(AppConstants.DEVICE_TOKEN,deviceToken)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun savePreference(key: String, value: Any) {
        deletePreference(key!!)
        when (value) {
            is Boolean -> sharedPreferences.edit().putBoolean(key, value).apply()
            is Int -> sharedPreferences.edit().putInt(key, value).apply()
            is Float -> sharedPreferences.edit().putFloat(key, value).apply()
            is Long -> sharedPreferences.edit().putLong(key, value).apply()
            is String -> sharedPreferences.edit().putString(key, value).apply()
            is Enum<*> -> sharedPreferences.edit().putString(key, value.toString()).apply()
            else -> throw RuntimeException("Attempting to save non-primitive preference")
        }

    }

    fun <T> getPreference(key: String): T {
        return sharedPreferences.all[key] as T
    }

    fun <T> getPreference(key: String, defValue: T): T {
        val returnValue = sharedPreferences.all[key] as T
        return returnValue ?: defValue
    }

    fun isPreferenceExists(key: String): Boolean {
        return sharedPreferences.contains(key)
    }

    var fcmToken: String?
        get() = sharedPreferences.getString("fcmToken", "")
        @SuppressLint("CommitPrefEdits")
        set(fcmToken) {
            editor = sharedPreferences.edit()
            editor!!.putString("fcmToken", fcmToken)
            editor!!.apply()
        }


    companion object {
        @JvmStatic
        var sInstance: PrefManager? = null
        @Synchronized
        @JvmStatic
        fun getInstance(context: Context?): PrefManager {
            if (sInstance == null) {
                sInstance = PrefManager(context)
            }
            return sInstance as PrefManager
        }
    }
}