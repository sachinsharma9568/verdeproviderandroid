package com.example.aircraftprovider.util

import android.content.Context

object CommonMethod {
    private const val MY_PREFS_NAME = "VerdeAircraft"

    /* public static void setFragment(Fragment fragment, boolean removeStack, FragmentActivity activity, int mContainer) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();
        if (removeStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ftTransaction.replace(mContainer, fragment);
        } else {
            ftTransaction.replace(mContainer, fragment);
            ftTransaction.addToBackStack(null);
        }
        ftTransaction.commit();
    }*/
    /*  public static void setSimpleFragment(Fragment fragment, FragmentActivity activity, int fromContainer) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(fromContainer, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }
*/
    fun setPreference(context: Context, key: String?, value: String?) {
        val editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getPreference(context: Context, key: String?): String? {
        val prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE)
        return prefs.getString(key, null)
    }

    fun clearSharedPreference(context: Context) {
        val editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }
}