package com.verde.aircraft.utils

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.lang.Exception

class NoInternetException(message : String ): IOException(message)
class ApiException(message: String) : IOException(message)

fun <T> T?.getRequestBody(): RequestBody {
    val result: T? = this
    if (result is File) {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), (result as File))
    } else if (result is String) {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), (result as String))
    }
    throw Exception("Invalid Format !! Pass String and File Only")
}