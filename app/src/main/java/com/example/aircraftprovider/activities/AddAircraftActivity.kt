package com.example.aircraftprovider.activities

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.provider.OpenableColumns
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.aircraftprovider.R
import com.example.aircraftprovider.image.PermissionCallback
import com.example.aircraftprovider.util.*
import com.example.aircraftprovider.viewHolder.AddCraftViewModel
import com.user.angelfood.provider.activityViewModel
import kotlinx.android.synthetic.main.activity_add_aircraft.*
import kotlinx.android.synthetic.main.header.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class AddAircraftActivity : AppCompatActivity(), KodeinAware, PermissionCallback {
    override val kodein by kodein()
    private val addCraftViewModel: AddCraftViewModel by activityViewModel()
    val prefManager: PrefManager by instance()
    private var updateFileAirWorth: File? = null
    private var updateFileRegistration: File? = null
    private var updateFileOperating: File? = null
    private var updateFileWeight: File? = null
    private val REQUEST_CODE_DOC = 101
    private var fileType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_aircraft)

        init()
        listener()
        objViewModel()
    }


    private fun listener() {
        ed_airworth.setOnClickListener {
            fileType = "1"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        ed_uploadRegistration.setOnClickListener {
            fileType = "2"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        ed_uploadLimitation.setOnClickListener {
            fileType = "3"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        ed_uploadweight.setOnClickListener {
            fileType = "4"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        btn_addaircraft.setOnClickListener {
            if (isValidation())
            updateFileAirWorth?.let { it1 ->
                updateFileRegistration?.let { it2 ->
                    updateFileOperating?.let { it3 ->
                        updateFileWeight?.let { it4 ->
                            addCraftViewModel.getAddCraft(
                                prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN, ""),
                                "1",
                                ed_worth.text.toString(),
                                ed_registration.text.toString(),
                                ed_operating.text.toString(),
                                ed_weight.text.toString(),
                                it1,
                                it2,
                                it3,
                                it4
                            )
                        }
                    }
                }
            }
        }
    }

    private fun isValidation(): Boolean {
        return true
    }

    private fun init() {
        header.title.text = getString(R.string.add_aircraft)
        header.ivBack.setOnClickListener { onBackPressed() }
    }

    private fun objViewModel() {
        addCraftViewModel.getAddCraftResponse().observe(this, {
            if (it.status == true) {
                showToast(it.message.toString())
                finish()
            }
        })

        addCraftViewModel.getErrorMessage().observe(this, {
            it?.let {
                showToast(it)
            }
        })

        addCraftViewModel.getLoadingStatus().observe(this, {
            if (it) ProgressBarUtils.showProgressDialog(this)
            else ProgressBarUtils.hideProgressDialog()
        })
    }

    override fun allPermissionGranted(isGranted: Boolean) {
        if (isGranted) {
            val intent = Intent()
            intent.type = "application/pdf"
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Pdf"), REQUEST_CODE_DOC)

        } else {
            showToast("Please enable all the permission")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_DOC && data != null) {

            var file: File? = null
            val uri = data.data
            Log.i("TAG", "onActivityResult: " + getFileName(uri))
            val parcelFileDescriptor: ParcelFileDescriptor
            try {
                parcelFileDescriptor =
                    uri?.let { getContentResolver().openFileDescriptor(it, "r", null) }!!
                val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
                file = File(getCacheDir(), getFileName(uri))
                val outputStream = FileOutputStream(file)
                IoUtils.copy(inputStream, outputStream)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            when (fileType) {
                "1" -> {
                    updateFileAirWorth = file
                }
                "2" -> {
                    updateFileRegistration = file
                }
                "3" -> {
                    updateFileOperating = file
                }
                "4" -> {
                    updateFileWeight = file
                }
            }
        }
    }

    fun getFileName(fileUri: Uri?): String? {
        var name = ""
        val returnCursor: Cursor =
            fileUri?.let { getContentResolver().query(it, null, null, null, null) }!!
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        name = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

}