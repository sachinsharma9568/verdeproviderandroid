package com.example.aircraftprovider.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityChangePasswordBinding
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager
import com.example.aircraftprovider.util.ProgressBarUtils
import com.example.aircraftprovider.util.showToast
import com.example.aircraftprovider.viewHolder.ResetPasswordViewModel

class ChangePasswordActivity : AppCompatActivity() {
    var binding: ActivityChangePasswordBinding? = null
    lateinit var viewmodel: ResetPasswordViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
        viewmodel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)

        setupViewModel()
        init()
        listener()
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        binding!!.header.title.text = "Change Password"
    }

    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }

        binding!!.btnchangepassword.setOnClickListener() {
            val password = binding!!.edPassword.text!!.trim().toString()
            val confirmPassword = binding!!.edConfirmpassword.text!!.trim().toString()

            if (isValidation(password, confirmPassword)) {
                callResetPasswordAPi(password, confirmPassword)
            }
        }
    }

    private fun callResetPasswordAPi(password: String, confirmPassword: String) {
        viewmodel.setData(
            PrefManager.getInstance(this).getPreference(AppConstant.EMAIL),
            password,
            confirmPassword
        )
    }

    private fun isValidation(password: String, confirmPassword: String): Boolean {
        if (TextUtils.isEmpty(password)) {
            showToast(resources.getString(R.string.enter_password))
            return false
        } else if (password.length < 7) {
            showToast(resources.getString(R.string.password_length))
            return false
        } else if (TextUtils.isEmpty(confirmPassword)) {
            showToast(resources.getString(R.string.enter_confirm_password))
            return false
        } else if (!password.equals(confirmPassword)) {
            showToast(getString(R.string.password_confirm_notmatched))
            return false
        }

        return true
    }

    private fun setupViewModel() {
        viewmodel.data.observe(this, { response ->
            if (response != null) {
                if (response.code == 200) {
                    showToast(response.message.toString())
                    PrefManager.getInstance(this).savePreference(AppConstant.KEY_IS_LOGIN, false)
                    startActivity(
                        Intent(
                            this,
                            LoginActivity::class.java
                        )
                    )
                    finish()
                }
            }
        })
        viewmodel.errorMessage.observe(this, { s ->
            if (s != null) {
                showToast(s)
            }
        })
        viewmodel.getLoadingStatusReset().observe(this, { aBoolean ->
            if (aBoolean!!) {
                ProgressBarUtils.showProgressDialog(this)
            } else {
                ProgressBarUtils.hideProgressDialog()
            }
        })
    }

}