package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityDocumentSection2Binding

class DocumentSection2Activity : AppCompatActivity() {
    var binding: ActivityDocumentSection2Binding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_section2)
        init()
        listener()
    }

    private fun init() {}
    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
        binding!!.tvSeeAll.setOnClickListener {
            startActivity(
                Intent(
                    this@DocumentSection2Activity,
                    AircraftDetailsActivity::class.java
                )
            )
        }
        binding!!.signupbtn.setOnClickListener {
            startActivity(
                Intent(
                    this@DocumentSection2Activity,
                    DocumentSection3Activity::class.java
                )
            )
        }
        binding!!.checkbox1.setOnClickListener {
            binding!!.checkbox1.isChecked = true
            binding!!.checkbox2.isChecked = false
            binding!!.checkbox3.isChecked = false
            binding!!.checkbox4.isChecked = false
        }
        binding!!.checkbox2.setOnClickListener {
            binding!!.checkbox2.isChecked = true
            binding!!.checkbox1.isChecked = false
            binding!!.checkbox3.isChecked = false
            binding!!.checkbox4.isChecked = false
        }
        binding!!.checkbox3.setOnClickListener {
            binding!!.checkbox3.isChecked = true
            binding!!.checkbox1.isChecked = false
            binding!!.checkbox2.isChecked = false
            binding!!.checkbox4.isChecked = false
        }
        binding!!.checkbox4.setOnClickListener {
            binding!!.checkbox4.isChecked = true
            binding!!.checkbox1.isChecked = false
            binding!!.checkbox2.isChecked = false
            binding!!.checkbox1.isChecked = false
        }
    }
}