package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityForgotPasswordBinding
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.hideProgressDialog
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.showProgressDialog
import com.example.aircraftprovider.util.Util.checkConnection
import com.example.aircraftprovider.util.Util.getShowToast
import com.example.aircraftprovider.viewHolder.ForgetPasswordViewModel

class ForgotPasswordActivity : AppCompatActivity() {
    var binding: ActivityForgotPasswordBinding? = null
    var mViewModel: ForgetPasswordViewModel? = null
    var Emails = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(
            ForgetPasswordViewModel::class.java
        )
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        init()
        listener()
        setupViewModel()
    }

    private fun setupViewModel() {
        mViewModel!!.data.observe(this, Observer { response ->
            if (response != null) {
                if (response.code == 200) {
                    getShowToast(this@ForgotPasswordActivity, response.message)
                    val intent = Intent(this@ForgotPasswordActivity, OtpActivity::class.java)
                    intent.putExtra("email", Emails)
                    startActivity(intent)
                    // startActivity(new Intent(ForgetPasswordActivity.this, VerificationCode.class));
                }
            }
        })
        mViewModel!!.errorMessage.observe(this, { s ->
            if (s != null) {
                getShowToast(this@ForgotPasswordActivity, s)
            }
        })
        mViewModel!!.getLoadingStatus().observe(this, Observer { aBoolean ->
            if (aBoolean == true) {
                showProgressDialog(this@ForgotPasswordActivity)
            } else {
                hideProgressDialog()
            }
        })
    }

    private fun init() {}
    private fun isvalidation(): Boolean {
        if (binding!!.edEmail.text.toString().isEmpty()) {
            getShowToast(this, resources.getString(R.string.enter_email))
            return false
        }
        return true
    }

    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
        binding!!.submitbtn.setOnClickListener { //  startActivity(new Intent(ForgotPasswordActivity.this,VerificationActivity.class));
            if (checkConnection(this@ForgotPasswordActivity)) {
                if (isvalidation()) {
                    Emails = binding!!.edEmail.text.toString().trim { it <= ' ' }
                    mViewModel!!.setData(Emails, AppConstant.Account_Type)
                }
            } else {
                getShowToast(
                    this@ForgotPasswordActivity,
                    resources.getString(R.string.check_internet)
                )
            }
        }
    }
}