package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivitySuccessfullyBinding

class SuccessfullyActivity : AppCompatActivity() {
    var binding: ActivitySuccessfullyBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_successfully)
        init()
        listener()
    }

    private fun init() {}
    private fun listener() {
        binding!!.ivCross.setOnClickListener { onBackPressed() }
    }
}