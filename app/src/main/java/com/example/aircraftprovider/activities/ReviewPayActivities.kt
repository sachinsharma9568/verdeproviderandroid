package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityReviewPayActivitiesBinding

class ReviewPayActivities : AppCompatActivity() {
    var binding: ActivityReviewPayActivitiesBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_review_pay_activities)
        init()
        listener()
    }

    private fun init() {}
    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
    }
}