package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.PagerAdapter
import com.example.aircraftprovider.databinding.ActivityTotalBookingBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener

class TotalBookingActivity : AppCompatActivity() {
    var binding: ActivityTotalBookingBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_total_booking)
        init()
        listener()
    }

    private fun init() {
        binding!!.header.title.text = "Total Booking"
        binding!!.tabLayout.addTab(binding!!.tabLayout.newTab().setText("Up Coming"))
        binding!!.tabLayout.addTab(binding!!.tabLayout.newTab().setText("Matching Travel Interest"))
        binding!!.tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = PagerAdapter(supportFragmentManager, binding!!.tabLayout.tabCount)
        binding!!.pager.adapter = adapter
        binding!!.pager.addOnPageChangeListener(TabLayoutOnPageChangeListener(binding!!.tabLayout))
        binding!!.tabLayout.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding!!.pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }
    }
}