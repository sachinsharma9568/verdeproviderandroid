package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityEditProfileBinding

class EditProfileActivity : AppCompatActivity() {
    var binding: ActivityEditProfileBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        init()
        listener()
    }

    private fun init() {}
    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
        binding!!.ivPencilpersonal.setOnClickListener {
            startActivity(
                Intent(
                    this@EditProfileActivity,
                    ChangeProfileActivity::class.java
                )
            )
        }
        binding!!.ivPencilpassword.setOnClickListener {
            startActivity(
                Intent(
                    this@EditProfileActivity,
                    ChangePasswordActivity::class.java
                )
            )
        }
    }
}