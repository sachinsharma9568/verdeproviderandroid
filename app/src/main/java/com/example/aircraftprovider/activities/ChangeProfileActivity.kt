package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityChangeProfileBinding
import com.example.aircraftprovider.util.*
import com.example.aircraftprovider.viewHolder.ProfileViewModel

class ChangeProfileActivity : AppCompatActivity() {
    var binding: ActivityChangeProfileBinding? = null
    lateinit var viewModel: ProfileViewModel
    var name: String = ""
    var email: String = ""
    var company_name: String = ""
    var mobile: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_profile)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        init()
        listener()
    }

    private fun init() {
        binding!!.header.title.text = "Change Profile"
    }

    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }
        binding!!.btnchangeprofile.setOnClickListener() {
            if (binding!!.edCompanyname.text.toString().isEmpty()) {
                showToast(getString(R.string.enter_company_name))
            } else if (binding!!.edMobilenumber.text.toString().isEmpty()) {
                showToast(getString(R.string.enter_mobile_number))
            } else if (binding!!.edMobilenumber.text.toString().length < 7) {
                showToast( resources.getString(R.string.mobile_lenght))
            } else if (binding!!.edName.text.toString().isEmpty()) {
                showToast(getString(R.string.enter_contact_name))
            } else {
                updateProfileDetails()
            }
        }

        getIntentData()
        setUpViewModel()

    }

    private fun getIntentData() {
        val intent = this.intent
        if (intent.extras != null) {
            name = intent.getStringExtra("name").toString()
            email = intent.getStringExtra("email").toString()
            mobile = intent.getStringExtra("mobile").toString()
            company_name = intent.getStringExtra("company_name").toString()
            binding?.edName?.setText(name)
            binding?.edEmail?.setText(email)
            binding?.edMobilenumber?.setText(mobile)
            binding?.edCompanyname?.setText(company_name)
        }
    }

    private fun setUpViewModel() {
        viewModel.getProfileLiveData.observe(this, { response ->
            if (response != null) {
                if (response.code == 200) {
                    showToast(response.message.toString())
                    finish()

                }
            }
        })

        viewModel.errorMessage.observe(this, { s: String? ->
            if (s != null) {
                showToast(s)
            }
        })

        viewModel.getLoadingStatus().observe(this, { aBoolean ->
            if (aBoolean!!) {
                ProgressBarUtils.showProgressDialog(this)
            } else {
                ProgressBarUtils.hideProgressDialog()
            }
        })
    }

    private fun updateProfileDetails() {
        viewModel.updateProfileDetails(
            PrefManager.getInstance(this).getPreference(AppConstant.KEY_AUTH_TOKEN, ""),
            binding!!.edName.text!!.trim().toString(),
            binding!!.edMobilenumber.text!!.trim().toString(),
            binding!!.edCompanyname.text!!.trim().toString()
        )
    }

}