package com.example.aircraftprovider.activities

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityCrewMemberDetailsBinding
import com.example.aircraftprovider.util.AppConstant
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.PDFView.Configurator
import com.github.barteksc.pdfviewer.listener.OnRenderListener
import kotlinx.android.synthetic.main.activity_crew_member_details.*
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection


class CrewMemberDetailsActivity : AppCompatActivity() {
    var binding: ActivityCrewMemberDetailsBinding? = null
    lateinit var pdfView: PDFView
    lateinit var pdfViewMedicalDoc: PDFView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_crew_member_details)
        init()
        listener()
    }

    private fun init() {
        binding!!.header.title.text = getString(R.string.crew_member_details)

        pdfView = findViewById<PDFView>(R.id.pdfViewCrewDoc)
        pdfViewMedicalDoc = findViewById<PDFView>(R.id.pdfViewMedicalDoc)

        if (intent.extras != null) {
            var id = intent.getStringExtra("id").toString()
            val name = intent.getStringExtra("name").toString()
            val certificate = intent.getStringExtra("certificate").toString()
            val othercertificate = intent.getStringExtra("othercertificate").toString()
            val medicaltill = intent.getStringExtra("medicaltill").toString()
            val crewDoc = intent.getStringExtra("crewDoc").toString()
            val medicalDoc = intent.getStringExtra("medicalDoc").toString()

            tvName.setText(name)
            tvCertificateMember.setText(certificate)
            tvCertificateOther.setText(othercertificate)
            tvCrewMedical.setText(medicaltill)

            val extension: String = crewDoc.substring(crewDoc.lastIndexOf("."))
            if (extension == ".pdf") {
                Log.e("document", "" + crewDoc)
                val crewUrl: String = AppConstant.IMAGE_BASE_URL_NEW + crewDoc
                Log.e("crewUrl","=="+crewUrl)
                RetrivePDFfromUrl(pdfView).execute(crewUrl)
            }

            val extensionMedical: String = medicalDoc.substring(crewDoc.lastIndexOf("."))
            if (extensionMedical == ".pdf") {
                Log.e("document", "" + crewDoc)
                val medicalDocURL: String = AppConstant.IMAGE_BASE_URL_NEW + medicalDoc
                Log.e("crewUrl","=="+medicalDocURL)
                RetrivePDFfromUrlMedical(pdfViewMedicalDoc).execute(medicalDocURL)
            }
        }
    }


    internal class RetrivePDFfromUrl(val pdfView: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfView.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }


    internal class RetrivePDFfromUrlMedical(val pdfViewMedicalDoc: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewMedicalDoc.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }

    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }
    }


}