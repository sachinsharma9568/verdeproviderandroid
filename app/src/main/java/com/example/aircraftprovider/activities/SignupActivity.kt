package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivitySignupBinding
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.hideProgressDialog
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.showProgressDialog
import com.example.aircraftprovider.util.Util.checkConnection
import com.example.aircraftprovider.util.Util.getShowToast
import com.example.aircraftprovider.util.Util.getValidateEmail
import com.example.aircraftprovider.viewHolder.SignUpViewModel

class SignupActivity : AppCompatActivity() {
    var binding: ActivitySignupBinding? = null
    var mViewModel: SignUpViewModel? = null
    var Emails = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        init()
        listener()
        setupViewModel()
    }

    private fun setupViewModel() {
        mViewModel!!.userRegister.observe(this, Observer { loginResponse ->
            if (loginResponse != null) {
                if (loginResponse.code == 200) {
                    getShowToast(this@SignupActivity, loginResponse.message)
                    binding!!.edCompanyname.setText("")
                    binding!!.edMobile.setText("")
                    binding!!.edContactname.setText("")
                    binding!!.edEmail.setText("")
                    binding!!.edPassword.setText("")
                    binding!!.edConfirmpassword.setText("")
                    val intent = Intent(this@SignupActivity, VerificationActivity::class.java)
                    intent.putExtra("email", Emails)
                    startActivity(intent)
                }
            }
        })

        mViewModel!!.errorMessage.observe(this, Observer { s: String? ->
            if (s != null) {
                getShowToast(this@SignupActivity, s)
            }
        })
        mViewModel!!.getLoadingStatus().observe(this, { aBoolean ->
            if (aBoolean!!) {
                showProgressDialog(this@SignupActivity)
            } else {
                hideProgressDialog()
            }
        })
    }

    private fun init() {}
    private fun listener() {
        binding!!.signupbtn.setOnClickListener { // startActivity(new Intent(SignupActivity.this,LoginActivity.class));
            if (checkConnection(this@SignupActivity)) {
                if (isValidation) {
                    signUpApi
                }
            } else {
                getShowToast(this@SignupActivity, resources.getString(R.string.check_internet))
            }
        }
        binding!!.tvLogin.setOnClickListener {
            startActivity(
                Intent(
                    this@SignupActivity,
                    LoginActivity::class.java
                )
            )
        }
        binding!!.tvReadmore.setOnClickListener {
            startActivity(
                Intent(
                    this@SignupActivity,
                    ContentActivity::class.java
                )
            )
        }
    }

    val signUpApi: Unit
        get() {
            Emails = binding!!.edEmail.text.toString().trim { it <= ' ' }
            mViewModel!!.setUserSignUp(AppConstant.Account_Type,
                binding!!.edContactname.text.toString().trim { it <= ' ' },
                binding!!.edEmail.text.toString().trim { it <= ' ' },
                binding!!.edPassword.text.toString().trim { it <= ' ' },
                binding!!.edConfirmpassword.text.toString().trim { it <= ' ' },
                AppConstant.Term_Condition,
                "asdfghjkkl",
                AppConstant.GetDeviceType,
                "37.4192",
                "-122.0574",
                binding!!.edMobile.text.toString().trim { it <= ' ' },
                binding!!.edCompanyname.text.toString().trim { it <= ' ' })
        }

    private val isValidation: Boolean
        get() {
            if (binding!!.edCompanyname.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_company_name))
                return false
            }
            if (binding!!.edMobile.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_mobile_number))
                return false
            }
            if (binding!!.edMobile.text.toString().length < 7) {
                getShowToast(this, resources.getString(R.string.mobile_lenght))
                return false
            }
            if (binding!!.edContactname.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_contact_name))
                return false
            }
            if (binding!!.edEmail.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_email))
                return false
            }
            if (!getValidateEmail(binding!!.edEmail.text.toString().trim { it <= ' ' })) {
                getShowToast(this, resources.getString(R.string.please_enter_email))
                return false
            }
            if (binding!!.edPassword.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_password))
                return false
            }
            if (binding!!.edPassword.text.toString().length < 8 && binding!!.edPassword.text.toString().length <= 16) {
                getShowToast(this, resources.getString(R.string.password_should_be_eight))
                return false
            }
            if (binding!!.edConfirmpassword.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_confrm_password))
                return false
            }
            if (binding!!.edConfirmpassword.text.toString().length < 8 && binding!!.edConfirmpassword.text.toString().length <= 16) {
                getShowToast(this, resources.getString(R.string.confrm_password_should_be_eight))
                return false
            }
            if (!binding!!.checkbox.isChecked) {
                getShowToast(this, resources.getString(R.string.accept_terms_and_condition))
                return false
            }
            if (binding!!.edPassword.text.toString() != binding!!.edConfirmpassword.text.toString()) {
                getShowToast(this, resources.getString(R.string.confrm_password_match))
                return false
            }
            return true
        }

    companion object {
        private const val TAG = "SignupActivity"
    }
}