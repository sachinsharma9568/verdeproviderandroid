package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityDocumentSection1Binding

class DocumentSection1Activity : AppCompatActivity() {
    var binding: ActivityDocumentSection1Binding? = null
    var a = arrayOf<String?>("Zip Code", "200 200", "201 201", "202 202")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_section1)
        init()
        listener()
    }

    private fun init() {
        val arrayAdapter: ArrayAdapter<*> = ArrayAdapter<Any?>(this, R.layout.spinner_layout, a)
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        binding!!.spinner.adapter = arrayAdapter
    }

    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
        binding!!.signupbtn.setOnClickListener {
            startActivity(
                Intent(
                    this@DocumentSection1Activity,
                    DocumentSection2Activity::class.java
                )
            )
        }
    }
}