package com.example.aircraftprovider.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityContentBinding

class ContentActivity : AppCompatActivity() {
    var binding: ActivityContentBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_content)

        binding!!.ivBack.setOnClickListener { v: View? -> finish() }
    }
}