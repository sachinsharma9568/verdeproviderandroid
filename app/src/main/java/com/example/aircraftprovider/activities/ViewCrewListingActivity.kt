package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.`interface`.onItemClicks
import com.example.aircraftprovider.adapter.ViewListingActivityAdapter
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager
import com.example.aircraftprovider.util.ProgressBarUtils
import com.example.aircraftprovider.util.showToast
import com.example.aircraftprovider.viewHolder.CrewMembersViewModel
import com.user.angelfood.provider.activityViewModel
import kotlinx.android.synthetic.main.activity_view_listing.*
import kotlinx.android.synthetic.main.header.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ViewCrewListingActivity : AppCompatActivity(), KodeinAware, onItemClicks {

    override val kodein by kodein()
    private val crewViewModel: CrewMembersViewModel by activityViewModel()
    val prefManager: PrefManager by instance()
    lateinit var crewAdapter: ViewListingActivityAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_listing)
        init()
        listener()
        objViewModel()
    }

    private fun objViewModel() {
        crewViewModel.getCrewMemberList().observe(this, {
            if (it.status == true) {
                if (it.data!!.size>0){
                    crewAdapter =
                        ViewListingActivityAdapter(this, it.data, this)
                    rv_viewlisting.layoutManager = LinearLayoutManager(this)
                    rv_viewlisting.adapter = crewAdapter
                }

            }
        })

        crewViewModel.getCrewDeleteLiveData().observe(this, {
            if (it.status == true) {
                crewViewModel.queryCrewListing(prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))

            }
        })

        crewViewModel.getErrorMessage().observe(this, {
            it?.let {
                showToast(it)
            }
        })

        crewViewModel.getLoadingStatus().observe(this, {
            if (it) ProgressBarUtils.showProgressDialog(this)
            else ProgressBarUtils.hideProgressDialog()
        })
    }

    private fun init() {
        crewViewModel.queryCrewListing(prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
    }

    private fun listener() {
        header.ivBack.setOnClickListener { onBackPressed() }
        header.title.setText(getString(R.string.view_listing))
    }

    override fun onClick(id: String) {
        callDeleteItem(id)
    }

    private fun callDeleteItem(id: String) {
        crewViewModel.queryCrewDeleteItem(id,prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
    }

}