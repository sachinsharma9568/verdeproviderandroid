package com.example.aircraftprovider.activities

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.aircraftprovider.R
import com.example.aircraftprovider.Sharedpreference.LoginPreferences.Companion.deleteAllPreference
import com.example.aircraftprovider.Sharedpreference.NoDeletePreferences
import com.example.aircraftprovider.activities.MainActivity
import com.example.aircraftprovider.databinding.ActivityMainBinding
import com.example.aircraftprovider.fragments.AccountFragment
import com.example.aircraftprovider.fragments.AddNewFragment
import com.example.aircraftprovider.fragments.HomeFragment
import com.example.aircraftprovider.fragments.ViewListingFragment
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager

class MainActivity : AppCompatActivity() {
    var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        init()
        listener()
    }

    private fun listener() {
        binding!!.ivmenu.setOnClickListener { v: View? ->
            if (!binding!!.drawerLayout.isDrawerOpen(
                    Gravity.LEFT
                )
            ) binding!!.drawerLayout.openDrawer(Gravity.LEFT) else binding!!.drawerLayout.closeDrawer(
                Gravity.LEFT
            )
        }
        binding!!.drawerMenu.constPendingaction.setOnClickListener {
            startActivity(
                Intent(
                    this@MainActivity,
                    PendingActionActivity::class.java
                )
            )
        }
        binding!!.drawerMenu.constTotalBooking.setOnClickListener {
            startActivity(
                Intent(
                    this@MainActivity,
                    TotalBookingActivity::class.java
                )
            )
        }
        binding!!.drawerMenu.constLogout.setOnClickListener { v: View? ->
            binding!!.drawerLayout.closeDrawer(GravityCompat.START)
            AlertDialog.Builder(this@MainActivity)
                .setIcon(R.drawable.logo)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.are_sure_logout))
                .setNeutralButton(getString(R.string.yes)) { dialog: DialogInterface?, which: Int ->
                    PrefManager.getInstance(this).savePreference(AppConstant.KEY_IS_LOGIN, false)
                    val i = Intent(this@MainActivity, LoginActivity::class.java)
                    startActivity(i)

                }
                .setNegativeButton(getString(R.string.no), null).show()
        }
        binding!!.drawerMenu.constAccount.setOnClickListener {
            startActivity(
                Intent(
                    this@MainActivity,
                    AccountActivity::class.java
                )
            )
        }
        binding!!.drawerMenu.constAccount.setOnClickListener {
            startActivity(
                Intent(
                    this@MainActivity,
                    AccountActivity::class.java
                )
            )
        }
    }

    private fun init() {
        loadHomeFragment()
        binding!!.bottomBar.setOnNavigationItemSelectedListener { menuItem: MenuItem ->
            val f = supportFragmentManager.findFragmentById(R.id.frameLayout)
            when (menuItem.itemId) {
                R.id.navHome -> {
                    if (f !is HomeFragment) {
                        loadHomeFragment()
                    }
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navScene -> {
                    if (f !is ViewListingFragment) {
                        setFragment(ViewListingFragment())
                    }
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navAction -> {
                    if (f !is AddNewFragment) {
                        setFragment(AddNewFragment())
                    }
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navAccount -> {
                    /*  if (f !is AccountFragment) {
                          setFragment(AccountFragment())
                      }*/
                    startActivity(Intent(this, AccountActivity::class.java))
                    return@setOnNavigationItemSelectedListener true
                }
            }
            true
        }
    }

    private fun loadHomeFragment() {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.frameLayout, HomeFragment())
        transaction.commit()
    }

    private fun setFragment(fragment: Fragment) {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.frameLayout, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }

    fun updateToolbarTitle(title: String?) {
        binding!!.toolbarTitle.text = title
    }

    fun setBottomBar(i: Int) {
        if (i == 0) {
            binding!!.bottomBar.menu.findItem(R.id.navHome).isChecked = true
        } else if (i == 1) {
            binding!!.bottomBar.menu.findItem(R.id.navScene).isChecked = true
        } else if (i == 2) {
            binding!!.bottomBar.menu.findItem(R.id.navAction).isChecked = true
        } else {
            binding!!.bottomBar.menu.findItem(R.id.navAccount).isChecked = true
        }
    }

    override fun onResume() {
        super.onResume()
        binding!!.drawerMenu.tvName.text =PrefManager.getInstance(this).getPreference(AppConstant.NAME)
        Glide.with(this)
            .load(AppConstant.IMAGE_BASE_URL+PrefManager.getInstance(this).getPreference(AppConstant.KEY_PROFILE, ""))
            .error(R.drawable.user_avtar)
            .into(binding!!.drawerMenu.imgheader)
    }
}