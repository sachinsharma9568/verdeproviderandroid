package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.`interface`.onItemClicks
import com.example.aircraftprovider.adapter.ViewListingPilotAdapter
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager
import com.example.aircraftprovider.util.ProgressBarUtils
import com.example.aircraftprovider.util.showToast
import com.example.aircraftprovider.viewHolder.PilotViewModel
import com.user.angelfood.provider.activityViewModel
import kotlinx.android.synthetic.main.fragment_view_pilot_listing.*
import kotlinx.android.synthetic.main.header.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ViewPilotListing : AppCompatActivity(), KodeinAware, onItemClicks {
    override val kodein by kodein()
    private val pilotViewModel: PilotViewModel by activityViewModel()
    val prefManager: PrefManager by instance()
    lateinit var pilotAdapter: ViewListingPilotAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_view_pilot_listing)
        
        initViews()
        objViewModels()
    }

    private fun objViewModels() {
        pilotViewModel.getPilotListingLiveData().observe(this, {
            if (it.status == true) {
                if (it.data!!.size>0){
                    pilotAdapter =
                        ViewListingPilotAdapter(this, it.data, this)
                    reyclerview.layoutManager = LinearLayoutManager(this)
                    reyclerview.adapter = pilotAdapter
                }
            }
        })

        pilotViewModel.getPilotDeleteLiveData().observe(this, {
            if (it.status == true) {
                pilotViewModel.queryPilotListing(prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
            }
        })

        pilotViewModel.getErrorMessage().observe(this, {
            it?.let {
                showToast(it)
            }
        })

        pilotViewModel.getLoadingStatus().observe(this, {
            if (it) ProgressBarUtils.showProgressDialog(this)
            else ProgressBarUtils.hideProgressDialog()
        })
    }

    private fun initViews() {
        header.ivBack.setOnClickListener { onBackPressed() }
        header.title.setText(getString(R.string.view_listing))
        pilotViewModel.queryPilotListing(prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
    }

    override fun onClick(id: String) {
        callDeleteItem(id)
    }

    private fun callDeleteItem(id: String) {
        pilotViewModel.queryPilotDeleteItem(id,prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
    }
}