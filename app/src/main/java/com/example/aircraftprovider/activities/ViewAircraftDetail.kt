package com.example.aircraftprovider.activities

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.aircraftprovider.R
import com.example.aircraftprovider.util.AppConstant
import com.github.barteksc.pdfviewer.PDFView
import kotlinx.android.synthetic.main.activity_view_aircraft_detail.*
import kotlinx.android.synthetic.main.header.view.*
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class ViewAircraftDetail : AppCompatActivity() {

    lateinit var pdfViewCrewDoc: PDFView
    lateinit var pdfViewRegistration: PDFView
    lateinit var pdfViewOperating: PDFView
    lateinit var pdfViewWeight: PDFView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_aircraft_detail)
        init()
        listener()
    }

    private fun init() {
        header.title.text = getString(R.string.aircraft_detail)
        pdfViewCrewDoc = findViewById(R.id.pdfViewCrewDoc)
        pdfViewRegistration = findViewById(R.id.pdfViewRegistration)
        pdfViewOperating = findViewById(R.id.pdfViewOperating)
        pdfViewWeight = findViewById(R.id.pdfViewWeight)

        if (intent.extras != null) {
            var id = intent.getStringExtra("id").toString()
            val airWorthnessCerti = intent.getStringExtra("airWorthnessCerti").toString()
            val registration = intent.getStringExtra("registration").toString()
            val operatingLimit = intent.getStringExtra("operatingLimit").toString()
            val weightBalance = intent.getStringExtra("weightBalance").toString()
            val uploadAirWorthnessCerti =
                intent.getStringExtra("uploadAirWorthnessCerti").toString()
            val uploadRegistration = intent.getStringExtra("uploadRegistration").toString()
            val uploadOperatingLimit = intent.getStringExtra("uploadOperatingLimit").toString()
            val uploadWeightBalance = intent.getStringExtra("uploadWeightBalance").toString()

            tvAirworth.setText(airWorthnessCerti)
            tvRegistration.setText(registration)
            tvOperatingLimits.setText(operatingLimit)
            tvWeight.setText(weightBalance)

            val extension: String =
                uploadAirWorthnessCerti.substring(uploadAirWorthnessCerti.lastIndexOf("."))
            if (extension == ".pdf") {
                Log.e("docuploadAirWorthness", "" + uploadAirWorthnessCerti)
                val uploadAirWorthnessURL: String =
                    AppConstant.IMAGE_BASE_URL_NEW + uploadAirWorthnessCerti
                Log.e("crewUrl", "==" + uploadAirWorthnessURL)
                RetrivePDFfromUrl(pdfViewCrewDoc).execute(uploadAirWorthnessURL)
            }

            val extensionOperatingLimit: String =
                uploadOperatingLimit.substring(uploadOperatingLimit.lastIndexOf("."))
            if (extensionOperatingLimit == ".pdf") {
                val operatingLimitURL: String =
                    AppConstant.IMAGE_BASE_URL_NEW + uploadOperatingLimit
                Log.e("operatingLimitURL", "==" + operatingLimitURL)
                RetrivePDFfromUrlMedical(pdfViewOperating).execute(operatingLimitURL)
            }

            val extensionReis: String =
                uploadRegistration.substring(uploadRegistration.lastIndexOf("."))
            if (extensionReis == ".pdf") {
                val RegistraDocURL: String = AppConstant.IMAGE_BASE_URL_NEW + uploadRegistration
                Log.e("RegistraDocURL", "==" + RegistraDocURL)
                RetrivePDFfromUrlRegistr(pdfViewRegistration).execute(RegistraDocURL)
            }

            val extensionWeight: String =
                uploadWeightBalance.substring(uploadWeightBalance.lastIndexOf("."))
            if (extensionWeight == ".pdf") {
                val weightURL: String = AppConstant.IMAGE_BASE_URL_NEW + uploadWeightBalance
                Log.e("weightURL", "==" + weightURL)
                RetrivePDFfromUrlWeight(pdfViewWeight).execute(weightURL)
            }

        }
    }

    private fun listener() {
        header.ivBack.setOnClickListener { onBackPressed() }
    }

    internal class RetrivePDFfromUrl(val pdfViewCrewDoc: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewCrewDoc.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }

    internal class RetrivePDFfromUrlMedical(val pdfViewOperating: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewOperating.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.responseCode === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }

    internal class RetrivePDFfromUrlRegistr(val pdfViewRegistration: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewRegistration.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }

    internal class RetrivePDFfromUrlWeight(val pdfViewWeight: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewWeight.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.responseCode === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }
}