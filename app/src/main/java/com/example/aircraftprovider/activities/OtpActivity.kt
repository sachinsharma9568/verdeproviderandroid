package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.OtpActivity
import com.example.aircraftprovider.databinding.ActivityOtpBinding
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.hideProgressDialog
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.showProgressDialog
import com.example.aircraftprovider.util.Util.checkConnection
import com.example.aircraftprovider.util.Util.getShowToast
import com.example.aircraftprovider.viewHolder.VerifyOtpViewModel
import com.mukesh.OnOtpCompletionListener

class OtpActivity : AppCompatActivity(), OnOtpCompletionListener {
    var binding: ActivityOtpBinding? = null
    var mviewmodel: VerifyOtpViewModel? = null
    var otpString : String? =""
    var email: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mviewmodel = ViewModelProviders.of(this).get(VerifyOtpViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp)
        init()
        listener()
        setupViewModel()
    }

    private fun setupViewModel() {
        mviewmodel!!.verifyOtp.observe(this, Observer { response ->
            Log.d("onSuccess=====", response!!.code.toString() + "-----Message---" + response.message)
            if (response.code == 200) {
                getShowToast(this@OtpActivity, response.message)
                val i = Intent(this@OtpActivity, ResetPasswordActivity::class.java)
                i.putExtra("email", email)
                startActivity(i)
            }
        })
        mviewmodel!!.errorMessage.observe(this, { s ->
            if (s != null) {
                getShowToast(this@OtpActivity, s)
            }
        })
        mviewmodel!!.getLoadingStatus().observe(this, Observer { aBoolean ->
            if (aBoolean!! == true) {
                showProgressDialog(this@OtpActivity)
            } else {
                hideProgressDialog()
            }
        })
    }

    private fun init() {
        binding!!.otpView.setOtpCompletionListener(this)
        val intent = intent
        if (intent != null) {
            email = intent.getStringExtra("email")
        }
    }

    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }

        binding!!.submitbtn.setOnClickListener { v: View? ->
            if(otpString!!.length==0){
                getShowToast(this@OtpActivity, resources.getString(R.string.otp_required))
            }
            else if (otpString!!.length<4){
                getShowToast(this@OtpActivity, resources.getString(R.string.enter_complete_otp))

            }else{
                if (checkConnection(this@OtpActivity)) {
                    mviewmodel!!.setVerifyOtp(email, binding!!.otpView.text.toString())
                } else {
                    getShowToast(this@OtpActivity, resources.getString(R.string.check_internet))
                }
            }

        }
    }

    override fun onOtpCompleted(otp: String) {
        otpString = otp
    }
}