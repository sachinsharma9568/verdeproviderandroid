package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.`interface`.onItemClicks
import com.example.aircraftprovider.adapter.ViewListingCraftAdapter
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager
import com.example.aircraftprovider.util.ProgressBarUtils
import com.example.aircraftprovider.util.showToast
import com.example.aircraftprovider.viewHolder.AddCraftViewModel
import com.user.angelfood.provider.activityViewModel
import kotlinx.android.synthetic.main.activity_view_listing.*
import kotlinx.android.synthetic.main.fragment_aircraft_listing.*
import kotlinx.android.synthetic.main.fragment_aircraft_listing.header
import kotlinx.android.synthetic.main.header.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ViewAircraftListing : AppCompatActivity(), KodeinAware , onItemClicks {
    override val kodein by kodein()
    private val aircraftViewModel: AddCraftViewModel by activityViewModel()
    val prefManager: PrefManager by instance()
    lateinit var craftAdapter: ViewListingCraftAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_aircraft_listing)

        initViews()
        objViewModel()
    }

    private fun objViewModel() {
        aircraftViewModel.getAddCraftListing().observe(this, {
            if (it.status == true) {
                if (it.data!!.size>0){
                    craftAdapter =
                        ViewListingCraftAdapter(this, it.data, this)
                    reyclerview.layoutManager = LinearLayoutManager(this)
                    reyclerview.adapter = craftAdapter
                }

            }
        })

        aircraftViewModel.getCrewDeleteLiveData().observe(this, {
            if (it.status == true) {
                aircraftViewModel.queryAirCraftListing(prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
            }
        })

        aircraftViewModel.getErrorMessage().observe(this, {
            it?.let {
                showToast(it)
            }
        })

        aircraftViewModel.getLoadingStatus().observe(this, {
            if (it) ProgressBarUtils.showProgressDialog(this)
            else ProgressBarUtils.hideProgressDialog()
        })
    }

    private fun initViews() {
        header.ivBack.setOnClickListener { onBackPressed() }
        header.title.setText(getString(R.string.view_listing))
        aircraftViewModel.queryAirCraftListing(prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
    }

    override fun onClick(id: String) {
        callDeleteItem(id)
    }

    private fun callDeleteItem(id: String) {
        aircraftViewModel.queryCraftDeleteItem(id,prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))
    }
}