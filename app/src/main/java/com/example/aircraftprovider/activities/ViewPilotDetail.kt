package com.example.aircraftprovider.activities

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.aircraftprovider.R
import com.example.aircraftprovider.util.AppConstant
import com.github.barteksc.pdfviewer.PDFView
import kotlinx.android.synthetic.main.activity_view_pilot_detail.*
import kotlinx.android.synthetic.main.header.view.*
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class ViewPilotDetail : AppCompatActivity() {
   lateinit var pdfViewPilotCerfi: PDFView
   lateinit var pdfViewMedicalCerfi: PDFView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pilot_detail)

        init()
    }

    private fun init() {

        pdfViewPilotCerfi = findViewById(R.id.pdfViewPilotCerfi)
        pdfViewMedicalCerfi = findViewById(R.id.pdfViewMedicalCerfi)
        header.title.setText(R.string.pilot_detail)
        header.ivBack.setOnClickListener { finish() }

        if (intent.extras != null) {
            var id = intent.getStringExtra("id").toString()
            val name = intent.getStringExtra("name").toString()
            val dob = intent.getStringExtra("dob").toString()
            val qualifiedPriv = intent.getStringExtra("qualifiedPriv").toString()
            val certificateNo = intent.getStringExtra("certificateNo").toString()
            val dateOfIssue = intent.getStringExtra("dateOfIssue").toString()
            val rating = intent.getStringExtra("rating").toString()
            val aircraftTypeRating = intent.getStringExtra("aircraftTypeRating").toString()
            val medicalCertiValid = intent.getStringExtra("medicalCertiValid").toString()
            val pilotCertification = intent.getStringExtra("pilotCertification").toString()
            val medicalCertification = intent.getStringExtra("medicalCertification").toString()

            tvFirstName.setText(name.split(" ")[0])
            tvLastName.setText(name.split(" ")[1])
            tvDob.setText(dob)
            tvQualified.setText(qualifiedPriv)
            tvCertificateNo.setText(certificateNo)
            tvIssueDate.setText(dateOfIssue)
            tvRating.setText(rating)
            tvAircraftRating.setText(aircraftTypeRating)
            tvMedicalCertiValid.setText(medicalCertiValid)

            val extension: String =
                pilotCertification.substring(pilotCertification.lastIndexOf("."))
            if (extension == ".pdf") {
                Log.e("docuploadAirWorthness", "" + pilotCertification)
                val uploadPilotCertificateURL: String =
                    AppConstant.IMAGE_BASE_URL_NEW + pilotCertification
                Log.e("crewUrl", "==" + uploadPilotCertificateURL)
                RetrivePDFfromUrl(pdfViewPilotCerfi).execute(uploadPilotCertificateURL)
            }

            val extensionMedical: String =
                medicalCertification.substring(medicalCertification.lastIndexOf("."))
            if (extensionMedical == ".pdf") {
                val medicalCertificateURL: String =
                    AppConstant.IMAGE_BASE_URL_NEW + medicalCertification
                Log.e("medicalCertificateURL", "==" + medicalCertificateURL)
                RetrivePDFfromUrlMedical(pdfViewMedicalCerfi).execute(medicalCertificateURL)
            }

        }

    }


    internal class RetrivePDFfromUrlMedical(val pdfViewMedicalCerfi: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewMedicalCerfi.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.responseCode === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }

    internal class RetrivePDFfromUrl(val pdfViewPilotCerfi: PDFView) :
        AsyncTask<String?, Void?, InputStream?>() {

        public override fun onPostExecute(inputStream: InputStream?) {
            pdfViewPilotCerfi.fromStream(inputStream).load()
        }

        public override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0])
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }


}