package com.example.aircraftprovider.activities

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.provider.OpenableColumns
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.aircraftprovider.R
import com.example.aircraftprovider.image.PermissionCallback
import com.example.aircraftprovider.util.*
import com.example.aircraftprovider.viewHolder.CrewMembersViewModel
import com.user.angelfood.provider.activityViewModel
import kotlinx.android.synthetic.main.activity_add_crew_members.*
import kotlinx.android.synthetic.main.header.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class AddCrewMemberActivity : AppCompatActivity(), KodeinAware, PermissionCallback {

    private val addCrewMembersViewModel: CrewMembersViewModel by activityViewModel()
    override val kodein by kodein()
    val prefManager: PrefManager by instance()
    private var updateCrewCerif: File? = null
    private var updateCrewMedicalCerti: File? = null
    private val REQUEST_CODE_DOC = 101
    private var fileType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_crew_members)

        init()
        listener()
        objViewModel()
    }

    private fun init() {

        header.title.text = getString(R.string.add_crew_member)
        header.ivBack.setOnClickListener { onBackPressed() }

        edtCrewMemberCerti.setOnClickListener {
            fileType = "1"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        edtMedicalCerti.setOnClickListener {
            fileType = "2"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

    }

    private fun listener() {
        btnAddCrew.setOnClickListener {
            val auth = PrefManager.getInstance(this).getPreference(AppConstant.KEY_AUTH_TOKEN, "")
            val fname = ed_fname.text.toString()
            val lname = ed_lname.text.toString()
            val certificate = edt_certificate.text.toString()
            val medicalValidation = edt_medical_valid.text.toString()
            val other_ceritificate = edt_other_certi.text.toString()

            if (isValidation(fname, lname, certificate, medicalValidation,other_ceritificate)) {
                addCrewMembersViewModel.getAddCraft(
                    auth,
                    fname,
                    lname,
                    certificate,
                    other_ceritificate,
                    medicalValidation,
                    updateCrewCerif!!,
                    updateCrewMedicalCerti!!
                )
            }
        }
    }

    private fun isValidation(
        fname: String,
        lname: String,
        certificate: String,
        medicalValidation: String,
        otherCeritificate: String
    ): Boolean {

        return true
    }

    private fun objViewModel() {
        addCrewMembersViewModel.getAddCrewMembers().observe(this, {
            if (it.status == true) {
                showToast(it.message.toString())
                finish()
            }
        })

        addCrewMembersViewModel.getErrorMessage().observe(this, {
            it?.let {
                showToast(it)
            }
        })

        addCrewMembersViewModel.getLoadingStatus().observe(this, {
            if (it) ProgressBarUtils.showProgressDialog(this)
            else ProgressBarUtils.hideProgressDialog()
        })
    }

    override fun allPermissionGranted(isGranted: Boolean) {
        if (isGranted) {
            val intent = Intent()
            intent.type = "application/pdf"
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Pdf"), REQUEST_CODE_DOC)

        } else {
            showToast("Please enable all the permission")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_DOC && data != null) {

            var file: File? = null
            val uri = data.data
            Log.i("TAG", "onActivityResult: " + getFileName(uri))
            val parcelFileDescriptor: ParcelFileDescriptor
            try {
                parcelFileDescriptor =
                    uri?.let { getContentResolver().openFileDescriptor(it, "r", null) }!!
                val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
                file = File(getCacheDir(), getFileName(uri))
                val outputStream = FileOutputStream(file)
                IoUtils.copy(inputStream, outputStream)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            when (fileType) {
                "1" -> {
                    updateCrewCerif = file
                }
                "2" -> {
                    updateCrewMedicalCerti = file
                }
            }
        }
    }

    fun getFileName(fileUri: Uri?): String? {
        var name = ""
        val returnCursor: Cursor =
            fileUri?.let { getContentResolver().query(it, null, null, null, null) }!!
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        name = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }


}