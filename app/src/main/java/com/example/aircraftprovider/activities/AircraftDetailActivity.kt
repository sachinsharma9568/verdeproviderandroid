package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.AircraftsDetailAdapter
import com.example.aircraftprovider.databinding.ActivityAircraftDetailsBinding

class AircraftDetailActivity : AppCompatActivity() {
    var binding: ActivityAircraftDetailsBinding? = null
    var a = arrayOf<Int>(R.drawable.plane2, R.drawable.plane3, R.drawable.plane1)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_aircraft_details)

        listener()
    }

    private fun listener() {
        binding?.rvSelectaircrafttype?.setLayoutManager(LinearLayoutManager(this,
            LinearLayoutManager.HORIZONTAL,true))
        binding!!.rvSelectaircrafttype.setAdapter(AircraftsDetailAdapter(this,a))
    }
}