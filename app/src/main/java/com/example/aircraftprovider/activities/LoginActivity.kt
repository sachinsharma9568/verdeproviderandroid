package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.aircraftprovider.R
import com.example.aircraftprovider.Sharedpreference.LoginPreferences
import com.example.aircraftprovider.activities.LoginActivity
import com.example.aircraftprovider.databinding.ActivityLoginBinding
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.hideProgressDialog
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.showProgressDialog
import com.example.aircraftprovider.util.Util.checkConnection
import com.example.aircraftprovider.util.Util.getShowToast
import com.example.aircraftprovider.util.Util.getValidateEmail
import com.example.aircraftprovider.viewHolder.LoginViewModel

class LoginActivity : AppCompatActivity() {
    var binding: ActivityLoginBinding? = null
    var mViewModel: LoginViewModel? = null
    lateinit var email: String
    lateinit var prefManager: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        prefManager = PrefManager.getInstance(this)

        listener()
        setupViewModel()
    }

    private fun setupViewModel() {
        mViewModel!!.data.observe(this, { loginResponse ->
            if (loginResponse?.code == 200) {
                binding!!.edEmail.setText("")
                binding!!.edPassword.setText("")
                prefManager.savePreference(AppConstant.KEY_IS_LOGIN, true)
                prefManager.savePreference(
                    AppConstant.KEY_AUTH_TOKEN,
                    loginResponse.data!!.token.toString()
                )
                prefManager.savePreference(AppConstant.EMAIL, loginResponse.data!!.email.toString())
                prefManager.savePreference(AppConstant.NAME, loginResponse.data!!.name.toString())
               // getShowToast(this@LoginActivity, loginResponse.message)
                getShowToast(this@LoginActivity, "You have logged in successfully")
                Log.e("auth token ", "" + prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN))

                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            } else {
                if (loginResponse!!.message.equals("Your account is not verifies")) {
                    val intent = Intent(this, VerificationActivity::class.java)
                    intent.putExtra("email", email)
                    startActivity(intent)
                }
            }
        })
        mViewModel!!.errorMessage.observe(this, { s ->
            if (s != null) {
                getShowToast(this@LoginActivity, s)
                Log.e("email", "==" + email)
            }
        })
        mViewModel!!.loadingStatus.observe(this, { aBoolean ->
            if (aBoolean) {
                showProgressDialog(this@LoginActivity)
            } else {
                hideProgressDialog()
            }
        })
    }

    val isValidation: Boolean
        get() {
            if (binding!!.edEmail.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_email))
                return false
            }
            if (!getValidateEmail(binding!!.edEmail.text.toString().trim { it <= ' ' })) {
                getShowToast(this, resources.getString(R.string.please_enter_email))
                return false
            }
            if (binding!!.edPassword.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_password))
                return false
            }
            return true
        }

    private fun listener() {
        binding!!.ivBack.setOnClickListener { v: View? -> onBackPressed() }
        binding!!.forgotpassword.setOnClickListener { v: View? ->
            startActivity(
                Intent(
                    this@LoginActivity,
                    ForgotPasswordActivity::class.java
                )
            )
        }
        binding!!.loginbtn.setOnClickListener { v: View? ->
            if (checkConnection(this@LoginActivity)) {
                if (isValidation) {
                    email = binding!!.edEmail.text.toString()
                    mViewModel!!.setData(
                        AppConstant.Account_Type,
                        binding!!.edEmail.text.toString(),
                        binding!!.edPassword.text.toString(),
                        "device_token",
                        AppConstant.GetDeviceType,
                        "37.4192",
                        "-122.0574"
                    )
                }
            } else {
                getShowToast(this@LoginActivity, resources.getString(R.string.check_internet))
            }
        }
        binding!!.tvSignup.setOnClickListener { v: View? ->
            startActivity(
                Intent(
                    this@LoginActivity,
                    SignupActivity::class.java
                )
            )
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        finish()
    }

    companion object {
        private const val TAG = "LoginActivity"
    }
}