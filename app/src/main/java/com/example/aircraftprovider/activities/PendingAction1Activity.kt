package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityPendingAction1Binding

class PendingAction1Activity : AppCompatActivity() {
    var binding: ActivityPendingAction1Binding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_action1)
        init()
        listener()
    }

    private fun init() {
        binding!!.header.title.text = "Pending Action"
    }

    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }
    }
}