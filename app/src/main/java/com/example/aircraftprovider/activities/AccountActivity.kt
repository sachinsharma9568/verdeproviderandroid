package com.example.aircraftprovider.activities

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityAccountBinding
import com.example.aircraftprovider.image.AddPhotoBottomDialogFragment
import com.example.aircraftprovider.image.AddPhotoCallback
import com.example.aircraftprovider.image.PermissionCallback
import com.example.aircraftprovider.util.*
import com.example.aircraftprovider.viewHolder.ProfileViewModel
import com.squareup.picasso.Picasso
import java.io.File

class AccountActivity : AppCompatActivity(), PermissionCallback {
    var binding: ActivityAccountBinding? = null
    lateinit var viewModel: ProfileViewModel
    private var uploadedImageFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_account)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        setOnClickItems()
    }


    private fun setOnClickItems() {
        binding!!.ivPencilpersonal.setOnClickListener {
            startActivity(
                Intent(this, ChangeProfileActivity::class.java).putExtra(
                    "name", binding!!.tvName.text.toString()
                ).putExtra(
                    "email",
                    binding!!.tvEmailid.text.toString()
                ).putExtra(
                    "mobile",
                    binding!!.tvMobile.text.toString()
                ).putExtra("company_name", binding!!.tvCompanyicon.text.toString())
            )
        }

        binding!!.ivPencilpassword.setOnClickListener() {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
        }

        binding!!.btnLogout.setOnClickListener() {
            AlertDialog.Builder(this)
                .setIcon(R.drawable.logo)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.are_sure_logout))
                .setNeutralButton(getString(R.string.yes)) { dialog: DialogInterface?, which: Int ->
                    PrefManager.getInstance(this).savePreference(AppConstant.KEY_IS_LOGIN, false)
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
                .setNegativeButton(getString(R.string.no), null).show()
        }

        binding!!.ivImageicon.setOnClickListener({
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.CAMERA)
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        })

        binding!!.ivBack.setOnClickListener {
            finish()
        }
    }


    override fun onResume() {
        super.onResume()
        callProfileApi()
        setUpViewModel()
    }

    private fun callProfileApi() {
        viewModel.getProfileData(
            PrefManager.getInstance(this).getPreference(AppConstant.KEY_AUTH_TOKEN, "")
        )
    }

    override fun allPermissionGranted(isGranted: Boolean) {
        if (isGranted) {
            val addPhotoBottomDialogFragment =
                AddPhotoBottomDialogFragment.newInstance(object : AddPhotoCallback {
                    override fun getImageFile(imageFile: File, imageUri: Uri) {
                        uploadedImageFile = imageFile
                        setImageInImageView(imageFile)
                    }
                })
            addPhotoBottomDialogFragment.show(
                this.supportFragmentManager,
                "add_photo_dialog_fragment"
            )

        } else {
            showToast("Please enable all the permission")
        }
    }

    private fun setImageInImageView(imageUri: File) {
      /*  Glide.with(this).load(imageUri)
            .into(binding!!.profileImage)*/

        Picasso.get()
            .load( imageUri)
            .error( R.drawable.profilepic )
            .placeholder( R.drawable.progress_animation )
            .into( binding!!.profileImage )

        viewModel.queryUpdateProfile(
            PrefManager.getInstance(this).getPreference(AppConstant.KEY_AUTH_TOKEN),
            uploadedImageFile
        )
    }


    private fun setUpViewModel() {
        viewModel.getProfileLiveData.observe(this, { response ->
            if (response != null) {
                if (response.code == 200) {
                    //  showToast(response.message.toString())
                    binding!!.tvName.setText(response.data!!.name.toString())
                    binding!!.tvEmailid.setText(response.data!!.email.toString())
                    binding!!.tvMobile.setText(response.data!!.mobile.toString())
                    binding!!.tvCompanyicon.setText(response.data!!.companyName.toString())
                    if (response.data!!.profileImage != null){

                        Picasso.get()
                            .load( AppConstant.IMAGE_BASE_URL + response.data!!.profileImage )
                            .error(R.drawable.profilepic )
                            .placeholder(R.drawable.progress_animation )
                            .into(binding!!.profileImage )
                    }
                      /*  Glide.with(this)
                            .load(AppConstant.IMAGE_BASE_URL + response.data!!.profileImage)
                            .error(R.drawable.profilepic).into(binding!!.profileImage)*/
                    PrefManager.getInstance(this).savePreference(AppConstant.NAME, response.data!!.name.toString())
                    PrefManager.getInstance(this).savePreference(AppConstant.KEY_PROFILE, response.data!!.profileImage.toString())
                }
            }
        })

        viewModel.errorMessage.observe(this, { s: String? ->
            if (s != null) {
                showToast(s)
            }
        })

        viewModel.getLoadingStatus().observe(this, { aBoolean ->
            if (aBoolean!!) {
                ProgressBarUtils.showProgressDialog(this)
            } else {
                ProgressBarUtils.hideProgressDialog()
            }
        })
    }
}