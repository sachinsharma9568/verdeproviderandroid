package com.example.aircraftprovider.activities

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.aircraftprovider.R
import com.example.aircraftprovider.image.PermissionCallback
import com.example.aircraftprovider.util.*
import com.example.aircraftprovider.viewHolder.PilotViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.user.angelfood.provider.activityViewModel
import kotlinx.android.synthetic.main.activity_add_pilot.*
import kotlinx.android.synthetic.main.activity_add_pilot.btn_addaircraft
import kotlinx.android.synthetic.main.activity_add_pilot.header
import kotlinx.android.synthetic.main.header.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class AddPilotActivity : AppCompatActivity(), KodeinAware, PermissionCallback {

    override val kodein by kodein()
    private val pilotViewModel: PilotViewModel by activityViewModel()
    val prefManager: PrefManager by instance()
    private val REQUEST_CODE_DOC = 101
    private var uploadCopyPilot: File? = null
    private var uploadCopyMedical: File? = null
    private var fileType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_pilot)
        init()
        listener()
        objViewModel()
    }

    private fun init() {
        header.title.setText(getString(R.string.add_pilot))



        btn_addaircraft.setOnClickListener {
            val auth = prefManager.getPreference(AppConstant.KEY_AUTH_TOKEN, "")
            val name = ed_fname.text.toString()
            val lname = ed_lname.text.toString()
            val dob = ed_dob.text.toString()
            val qualifiedPri = "1"
            val certificateNo = edt_certificate.text.toString()
            val issueDate = edt_isuuedate.text.toString()
            val medicalValidation = edt_medical_valid.text.toString()
            val aircraft_type_rating = "qwerty123"
            val ratingType = "1"

            if (isValidation()) {
                pilotViewModel.getAddPilotQuery(
                    auth,
                    name,
                    lname,
                    dob,
                    qualifiedPri,
                    issueDate,
                    certificateNo,
                    medicalValidation,
                    aircraft_type_rating,
                    ratingType,
                    uploadCopyMedical,
                    uploadCopyPilot
                )
            }
        }
    }

    private fun isValidation(): Boolean {
        return true
    }

    private fun listener() {
        header.ivBack.setOnClickListener { v: View? -> onBackPressed() }

        edt_pilot_certi.setOnClickListener {
            fileType = "1"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        edt_pilo.setOnClickListener {
            fileType = "2"
            val permissionArray = ArrayList<String>()
            permissionArray.apply {
                add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            PermissionManager(this, this, permissionArray)
        }

        ed_qualified.setOnClickListener{
            val btnsheet = layoutInflater.inflate(R.layout.bottom_qualified_layout, null)
            val dialog = BottomSheetDialog(this)
            dialog.setContentView(btnsheet)
            btnsheet.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }

        edt_rating.setOnClickListener {
            val btnsheet = layoutInflater.inflate(R.layout.bottom_rating_layout, null)
            val dialog = BottomSheetDialog(this)
            dialog.setContentView(btnsheet)
            btnsheet.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_DOC && data != null) {
            var file: File? = null
            val uri = data.data
            Log.i("TAG", "onActivityResult: " + getFileName(uri))
            val parcelFileDescriptor: ParcelFileDescriptor
            try {
                parcelFileDescriptor =
                    uri?.let { getContentResolver().openFileDescriptor(it, "r", null) }!!
                val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
                file = File(getCacheDir(), getFileName(uri))
                val outputStream = FileOutputStream(file)
                IoUtils.copy(inputStream, outputStream)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            when (fileType) {
                "1" -> {
                    uploadCopyPilot = file
                }
                "2" -> {
                    uploadCopyMedical = file
                }
            }
        }
    }


    private fun objViewModel() {
        pilotViewModel.getAddPilotLiveData().observe(this, {
            if (it.status == true) {
                showToast(it.message.toString())
                finish()
            }
        })

        pilotViewModel.getErrorMessage().observe(this, {
            it?.let {
                showToast(it)
            }
        })

        pilotViewModel.getLoadingStatus().observe(this, {
            if (it) ProgressBarUtils.showProgressDialog(this)
            else ProgressBarUtils.hideProgressDialog()
        })
    }

    override fun allPermissionGranted(isGranted: Boolean) {
        if (isGranted) {
            val intent = Intent()
            intent.type = "application/pdf"
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Pdf"), REQUEST_CODE_DOC)

        } else {
            showToast("Please enable all the permission")
        }
    }

    fun getFileName(fileUri: Uri?): String? {
        var name = ""
        val returnCursor: Cursor =
            fileUri?.let { getContentResolver().query(it, null, null, null, null) }!!
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        name = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

}