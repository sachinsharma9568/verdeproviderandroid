package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aircraftprovider.R
import com.example.aircraftprovider.databinding.ActivityDocumentSection3Binding

class DocumentSection3Activity : AppCompatActivity() {
    var binding: ActivityDocumentSection3Binding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_section3)
        init()
        listener()
    }

    private fun init() {}
    private fun listener() {
        binding!!.submitbtn.setOnClickListener {
            startActivity(
                Intent(
                    this@DocumentSection3Activity,
                    SuccessfullyActivity::class.java
                )
            )
        }
        binding!!.ivBack.setOnClickListener { onBackPressed() }
    }
}