package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.ResetPasswordActivity
import com.example.aircraftprovider.databinding.ActivityResetPasswordBinding
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.hideProgressDialog
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.showProgressDialog
import com.example.aircraftprovider.util.Util.checkConnection
import com.example.aircraftprovider.util.Util.getShowToast
import com.example.aircraftprovider.viewHolder.ResetPasswordViewModel

class ResetPasswordActivity : AppCompatActivity() {
    var binding: ActivityResetPasswordBinding? = null
    var mViewModel: ResetPasswordViewModel? = null
    var email: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(ResetPasswordViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)
        init()
        listener()
        setupViewModel()
    }

    private fun setupViewModel() {
        mViewModel!!.data.observe(this, Observer { response ->
            if (response != null) {
                if (response.code == 200) {
                    getShowToast(this@ResetPasswordActivity, response.message)
                    startActivity(Intent(this@ResetPasswordActivity, LoginActivity::class.java))
                }
            }
        })
        mViewModel!!.errorMessage.observe(this, { s ->
            if (s != null) {
                getShowToast(this@ResetPasswordActivity, s)
            }
        })
        mViewModel!!.getLoadingStatusReset().observe(this, Observer { aBoolean ->
            if (aBoolean!!) {
                showProgressDialog(this@ResetPasswordActivity)
            } else {
                hideProgressDialog()
            }
        })
    }

    private fun init() {
        val intent = intent
        if (intent != null) {
            email = intent.getStringExtra("email")
        }
    }

    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
        binding!!.submit.setOnClickListener { // startActivity(new Intent(ResetPasswordActivity.this,DocumentSection1Activity.class));
            if (checkConnection(this@ResetPasswordActivity)) {
                if (isValidation) {
                    mViewModel!!.setData(
                        email,
                        binding!!.edNewpassword.text.toString(),
                        binding!!.edConfirmpassword.text.toString()
                    )
                }
            } else {
                getShowToast(
                    this@ResetPasswordActivity,
                    resources.getString(R.string.check_internet)
                )
            }
        }
    }

    private val isValidation: Boolean
        private get() {
            if (binding!!.edNewpassword.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_password))
                return false
            }
            if (binding!!.edNewpassword.text.toString().length < 8 && binding!!.edNewpassword.text.toString().length <= 16) {
                getShowToast(this, resources.getString(R.string.password_should_be_eight))
                return false
            }
            if (binding!!.edConfirmpassword.text.toString().isEmpty()) {
                getShowToast(this, resources.getString(R.string.enter_confrm_password))
                return false
            }
            if (binding!!.edConfirmpassword.text.toString().length < 8 && binding!!.edConfirmpassword.text.toString().length <= 16) {
                getShowToast(this, resources.getString(R.string.confrm_password_should_be_eight))
                return false
            }
            if (binding!!.edNewpassword.text.toString() != binding!!.edConfirmpassword.text.toString()) {
                getShowToast(this, resources.getString(R.string.confrm_password_match))
                return false
            }
            return true
        }
}