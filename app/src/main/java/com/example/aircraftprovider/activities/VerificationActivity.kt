package com.example.aircraftprovider.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.aircraftprovider.R
import com.example.aircraftprovider.activities.VerificationActivity
import com.example.aircraftprovider.databinding.ActivityVerificationBinding
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.hideProgressDialog
import com.example.aircraftprovider.util.ProgressBarUtils.Companion.showProgressDialog
import com.example.aircraftprovider.util.Util.checkConnection
import com.example.aircraftprovider.util.Util.getShowToast
import com.example.aircraftprovider.viewHolder.VerificationViewModel

class VerificationActivity : AppCompatActivity() {

    var binding: ActivityVerificationBinding? = null
    var email: String? = null
    var mViewModel: VerificationViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(VerificationViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification)
        init()
        listener()
        setupViewModel()
    }

    private fun setupViewModel() {
        mViewModel!!.getVerificationResponse.observe(this, Observer { response ->
            if (response!!.code == 200) {
                getShowToast(this@VerificationActivity, response.message)
                val intent = Intent(this@VerificationActivity, LoginActivity::class.java)
                startActivity(intent)
            }
        })
        mViewModel!!.errorMessage.observe(this, { s ->
            if (s != null) {
                getShowToast(this@VerificationActivity, s)
            }
        })
        mViewModel!!.getLoadingStatus().observe(this, Observer { aBoolean ->
            if (aBoolean!!) {
                showProgressDialog(this@VerificationActivity)
            } else {
                hideProgressDialog()
            }
        })
    }

    private fun init() {
        val intent = intent
        if (intent.extras != null) {
            email = intent.getStringExtra("email").toString()
        }
        Log.e("email","=="+email)
        binding!!.tvEmail.text = email
    }

    private fun listener() {
        binding!!.ivBack.setOnClickListener { onBackPressed() }
        binding!!.sendbtn.setOnClickListener { //  startActivity(new Intent(VerificationActivity.this, ResetPasswordActivity.class));
            if (checkConnection(this@VerificationActivity)) {
                verificationApi
            } else {
                getShowToast(
                    this@VerificationActivity,
                    resources.getString(R.string.check_internet)
                )
            }
        }
    }

    val verificationApi: Unit
        get() {
            mViewModel!!.setUserVerification(email)
        }
}