package com.example.aircraftprovider.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.PendingActionAdapter
import com.example.aircraftprovider.databinding.ActivityPendingActionBinding
import com.example.aircraftprovider.model.ListModel
import java.util.*

class PendingActionActivity : AppCompatActivity() {
    var binding: ActivityPendingActionBinding? = null
    var listModels: MutableList<ListModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_action)
        init()
        listener()
    }

    private fun init() {
        binding!!.header.title.text = "Pending Action"
        binding!!.rvUpcomingrequest.layoutManager = LinearLayoutManager(this)
        binding!!.rvUpcomingrequest.adapter = PendingActionAdapter(this, listModels)
        listModels.clear()
        listModels.add(ListModel("06", "May"))
        listModels.add(ListModel("23", "June"))
        listModels.add(ListModel("10", "July"))
    }

    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }
    }
}