package com.example.aircraftprovider.activities

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.location.LocationManagerCompat
import com.example.aircraftprovider.R
import com.example.aircraftprovider.Sharedpreference.LoginPreferences
import com.example.aircraftprovider.Sharedpreference.NoDeletePreferences
import com.example.aircraftprovider.activities.AllowLocationActivity
import com.example.aircraftprovider.util.AppConstant
import com.example.aircraftprovider.util.PrefManager

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        checkLocation()

        Handler().postDelayed({
            if (PrefManager.getInstance(this).getPreference(AppConstant.KEY_IS_LOGIN, false)) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                if (checkLocation()) {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this, AllowLocationActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }

        }, 3000)
    }

    private fun checkLocation(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return LocationManagerCompat.isLocationEnabled(locationManager)
    }
}