package com.example.aircraftprovider.activities

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aircraftprovider.R
import com.example.aircraftprovider.adapter.UpcomingRequestActivityAdapter
import com.example.aircraftprovider.databinding.ActivityUpcomingRequestBinding
import com.example.aircraftprovider.model.ListModel
import java.util.*

class UpcomingRequestActivity : AppCompatActivity() {
    var binding: ActivityUpcomingRequestBinding? = null
    var listModels: MutableList<ListModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_upcoming_request)
        init()
        listener()
    }

    @SuppressLint("SetTextI18n")
    private fun listener() {
        binding!!.header.ivBack.setOnClickListener { onBackPressed() }
        binding!!.header.title.text = "Upcoming Request"
    }

    private fun init() {
        binding!!.rvUpcomingrequest.layoutManager = LinearLayoutManager(this)
        binding!!.rvUpcomingrequest.adapter = UpcomingRequestActivityAdapter(this, listModels)
        listModels.clear()
        listModels.add(ListModel("06", "May"))
        listModels.add(ListModel("23", "June"))
        listModels.add(ListModel("10", "July"))
    }
}